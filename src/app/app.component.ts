import { Component } from '@angular/core';
import { distinctUntilChanged } from 'rxjs/operators';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  isAuth: boolean = false;
  constructor(
    private authService: AuthService,
    private userService: UserService,
  ) {
    userService.fetch();
    userService.authenticated.pipe(distinctUntilChanged()).subscribe(auth => {
      this.isAuth = auth;
    });
  }
}
