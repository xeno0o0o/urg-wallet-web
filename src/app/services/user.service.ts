import { Injectable } from '@angular/core';
import { distinctUntilChanged } from 'rxjs/operators';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { ApiService } from './api.service';
import { EndpointService } from './endpoint.service';
import jwt_decode from 'jwt-decode';

const VALID_FIELDS = 'id';
const parseJwt = (token: any) => {
  try {
    const parsed = JSON.parse(atob(token.split('.')[1]));
    const valid: any = {};
    Object.keys(parsed).filter((key: any) => VALID_FIELDS.split(',').includes(key)).forEach(key => valid[key] = parsed[key]);
    return valid;
  } catch (e) {
    return null;
  }
};
const expiration = (token: any) => {
  try {
    const parsed = JSON.parse(atob(token.split('.')[1]));
    return parsed[`exp`];
  } catch (e) {
    return null;
  }
};

@Injectable({ providedIn: 'root' })
export class UserService {
  currentUserSubject = new BehaviorSubject<any>({} as any);
  public userInfo = new Subject();
  public currentUser = this.currentUserSubject
    .asObservable()
    .pipe(distinctUntilChanged());
  public authenticationState = new BehaviorSubject<boolean>(false);

  public authenticated = this.authenticationState
    .asObservable()
    .pipe(distinctUntilChanged());

  constructor(
    private endpointService: EndpointService,
  ) { }

  fetch(): void {
    this.currentUserSubject.next(parseJwt(localStorage.getItem('access_token')));
    this.setAuthenticated();
  }

  setUser(token: string): void {
    this.currentUserSubject.next(parseJwt(token));
    this.setAuthenticated();
    this.setUserInfo();
  }
  
  setAuthenticated(): void {
    const now = new Date();
    if (localStorage.getItem('access_token')) {
      this.authenticationState.next(now.getTime() / 1000 < this.expiration(localStorage.getItem('access_token')));
      this.setUserInfo();
    } else {
      this.authenticationState.next(false);
    }
  }

  parseJwt(token: any): any {
    const parsed: any = jwt_decode(token);
    const valid: any = {};
    Object.keys(parsed).filter(key => VALID_FIELDS.split(',').includes(key)).forEach((key: string) => valid[key] = parsed[key]);
    return valid;
  }

  expiration(token: any): any {
    const parsed: any = jwt_decode(token);
    return parsed[`exp`];
  }

  logout(): void {
    this.currentUserSubject.observers = [];
    this.authenticationState.next(false);
  }

  getAuthenticationState(): Observable<any> {
    return this.authenticationState
      .asObservable()
      .pipe(distinctUntilChanged());
  }

  getToken() {
    this.getParseToken(localStorage.getItem('access_token'));
  }

  public getRefreshToken(): Observable<string> {
    const token: string = this.refreshTokenFromLoc(localStorage.getItem('refresh_token'));
    return of(token);
  }

  refreshTokenFromLoc(token: any) {
    return token;
  }


  getParseToken(token: any) {
    const parsed: any = jwt_decode(token);
    return parsed;
  }

  setUserInfo(): void {
    this.endpointService.getUserInfo().subscribe(usr => {
      this.userInfo.next(usr);
    });
  }

}

