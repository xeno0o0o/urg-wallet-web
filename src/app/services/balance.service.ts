import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { EndpointService } from './endpoint.service';


const AUTH_URL = environment.url;

@Injectable({ providedIn: 'root' })
export class BalanceService {
  constructor(
      private endpointService: EndpointService
  ) { }

  getURGbyDax():any {
    const data = {
      "query": "query ActivePairs {\n  sys_pair(where: {is_active: {_eq: true}}, order_by: {id: asc}) {\n    id\n    symbol\n    base_max_size\n    base_min_size\n    base_tick_size\n    quote_max_size\n    quote_min_size\n    quote_tick_size\n    baseAsset {\n      id\n      code\n      name\n      scale\n      is_crypto\n      __typename\n    }\n    quoteAsset {\n      id\n      code\n      name\n      scale\n      __typename\n    }\n    price {\n      last_price\n      last_price_dt\n      __typename\n    }\n    stats24 {\n      high\n      low\n      change24h\n      vol\n      __typename\n    }\n    __typename\n  }\n}\n"
    }
    return this.endpointService.getURGbyDax(data);
  }

  getUserMNT():any {
    return this.endpointService.fiatBalance();
  }

}
