import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { Observable, of, throwError } from "rxjs";
import { catchError, switchMap, tap } from 'rxjs/operators';
import { API_FORGOT, API_LOGIN, API_REGIST, AUTH_URL, WEB_ACCESS } from "../shared/uri.constants";

import { UserService } from "./user.service";
import { ApiService } from "./api.service";

const TOKEN_DATA = 'grant_type=#g';
const LOGIN_DATA = 'username=#u&password=#p&grant_type=#g';
const REFRESH_DATA = 'grant_type=#g&refresh_token=#t';

@Injectable({ providedIn: 'root' })
export class AuthService implements AuthService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private apiService: ApiService
  ) { }

  public getAccessToken(): Observable<string | null> {
    return of(localStorage.getItem('access_token'));
  }

  public refreshToken(): Observable<any> {
    let refreshToken = this.userService.getRefreshToken();
    let formData = REFRESH_DATA.replace('#g', 'refresh_token');
    

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa(WEB_ACCESS),
      })
    };
    return this.userService.getRefreshToken().pipe(
        switchMap((refreshToken: string) => {
            formData = formData.replace('#t', refreshToken);
            return this.http.post(AUTH_URL, formData, httpOptions);
          }
        ),
        tap((tokens: any) => {
          this.saveAccessData(tokens)
          this.setRefreshTokenData(tokens);
          this.userService.setUser(tokens[`access_token`]);
        }),
        catchError((err) => {
          this.logout();
          return throwError(err);
        })
      );
  }

  

  public getToken(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa(WEB_ACCESS),
      })
    };
    let formData = TOKEN_DATA.replace('#g', 'client_credentials');
    return this.http.post(AUTH_URL, formData, httpOptions).pipe(
      tap((tokens: any) => {
        for (const i in tokens) {
          localStorage.setItem(i, tokens[i]);
        }
      }),
      catchError((error) => throwError(error.error))
    );
  }

  public login(form: { username: string, password: string }): Observable<any> {
    let formData = LOGIN_DATA.replace('#u', form.username);
    formData = formData.replace('#p', form.password);
    formData = formData.replace('#g', 'password');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa(WEB_ACCESS),
      })
    };
    return this.http.post(API_LOGIN, formData, httpOptions).pipe(
      tap((tokens: any) => {
        this.saveAccessData(tokens);
        this.setRefreshTokenData(tokens);
        this.userService.setUser(tokens[`access_token`]);
      }),
      catchError((error) => throwError(error.error))
    );
  }

  public setRefreshTokenData(tokenData: any) {
    const jwtToken = this.userService.getParseToken(tokenData.access_token);

    const expires = new Date(jwtToken.exp * 1000);
    const timeout = expires.getTime() - Date.now() - 60 * 1000;

    localStorage.setItem('exp_d', expires.toString())
    
  }

  public isTokenAviable(): boolean {
    if (localStorage.getItem('exp_d')) {
      const data: any = localStorage.getItem('exp_d');
      var now = new Date(Date.now())
      const exp =  new Date(data);
      return exp > now ? true : false;
    } else {
      return false;
    }
  }

  public logout(): void {
    localStorage.clear();
    this.userService.logout();
    this.router.navigate(['/nft/list']).then();
  }

  public saveAccessData(accessData: any): void {
    for (const i in accessData) {
      localStorage.setItem(i, accessData[i]);
    }
  }

  public regist(form: { phone: string, password: string }) {
    const data = {
      phoneNo: form.phone,
      pwd: form.password
    };
    return this.apiService.post(API_REGIST, data).pipe(
      tap((res: any) => {
        if (res && res.status === 0) {
        }
      }),
      catchError((error) => throwError(error.error))
    );
  }

  public forgotPassword(phone: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + btoa(WEB_ACCESS),
      })
    };
    const data = {
      phoneNo: phone
    }
    return this.http.post(API_FORGOT, data, httpOptions).pipe(
      tap((res: any) => { }),
      catchError((error) => throwError(error.error))
    );
  }
}
