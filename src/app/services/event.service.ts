import { Injectable } from "@angular/core";
import { Subject } from "rxjs";


@Injectable({ providedIn: 'root' })
export class EventService {
  public giftsEmit = new Subject();
  public walletMenu = new Subject();
  public headerMenu = new Subject();
  public isSaveProfule = new Subject();
  public giftList: any = [];
  public nftList: any = [];
  public user: any = [];

  setGifts(giftList: any) {
    this.giftsEmit.next(giftList);
  }

  setGiftList(data: any) {
    this.giftList = data;
  }
  getGiftList() {
    return this.giftList;
  }
  setNFTList(data: any) {
    this.nftList = data;
  }
  getNFTList() {
    return this.nftList;
  }
  walletMenuChange(tabIndex: any) {
    this.walletMenu.next(tabIndex);
  }
  headerMenuChange(tabIndex: number) {
    this.headerMenu.next(tabIndex);
  }
  isSaveProfile(data: any) {
    this.isSaveProfule.next(data); 
  }
  
  setUser(data: any) {
    this.user = data;
  }
  getUser() {
    return this.user;
  }
}
