import { Injectable } from '@angular/core';
import { EndpointService } from './endpoint.service';

@Injectable({ providedIn: 'root' })
export class FileService {
  constructor(
    private endpointService: EndpointService
  ) { }

  getImage(filePath: string) {
    return this.endpointService.fileDownload(filePath);
  }
}
