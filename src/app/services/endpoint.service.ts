import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { Observable } from 'rxjs/internal/Observable';
import { API_ADD_BANKS, API_ALL_FIATS, API_BANNER_TOP, API_BUY_CHECK, API_BUY_GIFT, API_DELETE_BANKS, API_FIAT_BALANCE, API_FIAT_BALANCE_CHARGE, API_FIAT_BANKS, API_FIAT_DISCHARGE, API_FILE_DOWNLOAD, API_FORGOT, API_GET_COLLECTION, API_GET_CREATER, API_GET_CREATOR_NFT, API_GIFT, API_KYC_BACK, API_KYC_FRONT, API_KYC_SELFIE, API_ME, API_MNT_BALANCE, API_MNT_BALANCE_HISTORY, API_MY_BANKS, API_NFT_ACTION_COUNT, API_NFT_BUY, API_NFT_CAT, API_NFT_DETAIL, API_NFT_HISTORY, API_NFT_LIST, API_NFT_MINE, API_NFT_OWNERS, API_NFT_PROFILE_UPDATE, API_NFT_PUBLIC, API_NFT_UPLOAD_COVER, API_NFT_UPLOAD_PROFILE, API_OWNED_GIFT, API_PROFILE_CHANGE_PASS, API_REGIST_CONFIRM, API_SEND_GIFT, API_TAN_AGAIN, API_USER_CONFIRM_INFO, API_USER_URG_BALANCE, API_USE_GIFT, API_V2_BUY_CHECK, API_V2_BUY_GIFT, API_V2_GIFT, API_V2_GIFT_DETAIL, API_WALLET_INT, API_WITHDRAW, API_WITHDRAW_HISTORY, API_WITHDRAW_TAN, API_WITHDRAW_TOKEN_TO_ADDRESS, URG_DAX_VAL } from '../shared/uri.constants';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class EndpointService {
  constructor(
    private api: ApiService,
    private http: HttpClient,
    private handler: HttpBackend,
  ) { }

  public formatErrors(error: any): any {
    return throwError(error && error.error ? error.error : error);
  }

  getUserInfo() {
    return this.api.get(API_ME);
  }

  tanAgain(data: any): Observable<any> {
    return this.api.post(API_TAN_AGAIN, data);
  }

  registConfirm(phone: string, tan: string): Observable<any> {
    const data = {
      phoneNo: phone,
      tanCode: tan
    }
    return this.api.post(API_REGIST_CONFIRM, data);
  }

  forgetPassword(phone: any) {
    return this.api.post(API_FORGOT, phone);
  }

  getGift(data: any) {
    return this.api.get(API_V2_GIFT, data);
  }

  getGiftDetail(giftId: any) {
    return this.api.get(API_V2_GIFT_DETAIL, giftId);
  }

  fileDownload(filePath: string): Observable<any> {
    return this.http.get(API_FILE_DOWNLOAD + filePath, { observe: 'response', responseType: 'blob' });
  }

  kycFront(data: any) {
    return this.api.postImage(API_KYC_FRONT, data);
  }

  kycBack(data: any) {
    return this.api.postImage(API_KYC_BACK, data);
  }

  kycSelfie(data: any) {
    return this.api.postImage(API_KYC_SELFIE, data);
  }

  getUserUrgBalance(): Observable<any> {
    return this.api.get(API_USER_URG_BALANCE);
  }

  profileChangePass(profileModal: any): Observable<any> {
    return this.api.post(API_PROFILE_CHANGE_PASS, profileModal);
  }

  confirmUserInfo(userInfo: any) {
    return this.api.post(API_USER_CONFIRM_INFO, userInfo);
  }

  fiatBalance() {
    return this.api.get(API_FIAT_BALANCE);
  }

  fiatBalanceCharge() {
    return this.api.get(API_FIAT_BALANCE_CHARGE);
  }

  getWalletIns() {
    return this.api.get(API_WALLET_INT);
  }

  getOwnedGift(page:any) {
    return this.api.get(API_OWNED_GIFT,page);
  }

  useGiftCard(data: any) {
    return this.api.get(API_USE_GIFT, data);
  }

  sendGiftCard(obj: any) {
    return this.api.get(API_SEND_GIFT, obj)
  }

  buyGiftCard(obj: any) {
    return this.api.get(API_BUY_GIFT, obj);
  }

  buyGiftCardV2(obj: any) {
    return this.api.post(API_V2_BUY_GIFT, obj);
  }

  buyGiftCheck(queryId: any) {
    return this.api.get(API_BUY_CHECK, queryId)
  }

  buyGiftCheckV2(inquireId: any) {
    return this.api.get(API_V2_BUY_CHECK, inquireId);
  }

  sendWithDraw(withDrawObj: any) {
    return this.api.post(API_WITHDRAW, withDrawObj);
  }

  withdrawTan() {
    return this.api.get(API_WITHDRAW_TAN);
  }

  transferTokenToAddress(obj: any) {
    return this.api.post(API_WITHDRAW_TOKEN_TO_ADDRESS, obj);
  }

  getWithdrawHistory(coinCode: string, page: any) {
    return this.api.get(API_WITHDRAW_HISTORY + '/' + coinCode, page);
  }

  getMyBankList() {
    return this.api.get(API_MY_BANKS);
  }

  getBanks() {
    return this.api.get(API_FIAT_BANKS);
  }

  deleteBank(bank: any) {
    return this.api.delete(API_DELETE_BANKS, bank);
  }

  getFiats() {
    return this.api.get(API_ALL_FIATS);
  }

  dischargeBalance(data: any) {
    return this.api.post(API_FIAT_DISCHARGE, data);
  }

  getAddBank(data: any) {
    return this.api.post(API_ADD_BANKS, data);
  }

  getMNTbalance() {
    return this.api.get(API_MNT_BALANCE);
  }

  getMNTbalanceHistory(data: any) {
    return this.api.get(API_MNT_BALANCE_HISTORY, data);
  }

  updateNFTprofile(data: any) {
    return this.api.post(API_NFT_PROFILE_UPDATE, data);
  }

  uploadNFTprofile(data: any) {
    return this.api.postImage(API_NFT_UPLOAD_PROFILE, data);
  }

  uploadNFTcover(data: any) {
    return this.api.postImage(API_NFT_UPLOAD_COVER, data);
  }

  getCreatorAbout(creatorNickName: string) {
    return this.api.get(API_GET_CREATER + creatorNickName);
  }  

  getURGbyDax(body: any) {
    this.http = new HttpClient(this.handler);
    let headers = new HttpHeaders();
    headers = headers.append('Access-Control-Allow-Origin', '*');
    return this.http
      .post(URG_DAX_VAL, body, {headers});

  }

  getNftList(data: any) {
    return this.api.get(API_NFT_LIST, data);
  }

  getNftCat() {
    return this.api.get(API_NFT_CAT);
  }

  getNftDetail(bsId: any, code: any) {
    return this.api.get(API_NFT_DETAIL + bsId + '/' + code);
  }

  getNftActionCount(data: any) {
    return this.api.get(API_NFT_ACTION_COUNT, data);
  }

  getNftDetailOwners(bsId: any, code: any, page: any) {
    return this.api.get(API_NFT_OWNERS + bsId + '/' + code + '/owners', page);
  }

  getCollection(data: any) {
    return this.api.get(API_GET_COLLECTION, data);
  }

  getCreatorNfts(nickname: any) {
    return this.api.get(API_GET_CREATOR_NFT, nickname);
  }

  buyNft(data: any) {
    return this.api.post(API_NFT_BUY, data);
  }

  getNftMine(data: any) {
    return this.api.get(API_NFT_MINE, data)
  }

  getNftHistory(bsId: string, code: string, page: any) {
    return this.api.get(API_NFT_HISTORY + bsId + '/' + code + '/history', page)
  }

  getPublicNft(data: any) {
    return this.api.get(API_NFT_PUBLIC, data);
  }

  getBannerTop() {
    return this.api.get(API_BANNER_TOP);
  }
}
