import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as uuid from 'uuid';


const AUTH_URL = environment.url;

@Injectable({ providedIn: 'root' })
export class UuidService {
  constructor() { }

  getUUID() {
    return uuid.v4();
  }
}
