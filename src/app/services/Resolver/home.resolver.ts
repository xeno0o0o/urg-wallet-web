import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { EndpointService } from '../endpoint.service';

@Injectable({ providedIn: 'root' })
export class HomeResolver implements Resolve<any[]> {
  data: any = {};
  constructor(
    private endpointService: EndpointService
  ) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const data = {
      'page': 0,
      'size': 8
    }
    return this.endpointService.getGift(data);
  }
}
