import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { EndpointService } from '../endpoint.service';

@Injectable({ providedIn: 'root' })
export class NftResolver implements Resolve<any[]> {

  data: any = [];
  constructor(
    private endpointService: EndpointService
  ) {
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const data = {
      'category-ids': '1,2,3',
      'page': 0,
      'size': 10
    }
    return this.endpointService.getNftList(data);
  }
}
