import { environment } from '../../environments/environment';

export const HOST = environment.url;
export const WEB_ACCESS = `web:QwGg^u/'N9-fS!-M`;
export const BLOCK_DEC = 1e+18;
export const AUTH_URL = HOST + '/uaa/oauth/token';

export const bsNetworkId = 2; // prod 2 test => 1
export const IMG_PATH = HOST + '/file/v1/d/';
export const IMG_LCL_PATH = '../../../assets/images/';

export const URG_DAX_VAL = 'https://api.dax.mn/v1/graphql';

export const API_LOGIN = HOST + '/uaa/oauth/token';
export const API_REGIST = '/app/v1/user/register';
export const API_FORGOT = '/app/v1/user/forgot-pwd';

export const API_TAN_AGAIN = '/app/v1/user/register-tan-again';
export const API_REGIST_CONFIRM = '/app/v1/user/register-confirm';
export const API_PROFILE_CHANGE_PASS = '/app/v1/user/change-pwd';
export const API_ME = '/app/v1/user/me';
export const API_USER_URG_BALANCE = '/blockchain/v1/erc20/balance';
export const API_USER_CONFIRM_INFO = '/app/v1/user/change-info';

export const API_WALLET_INT = '/app/v1/wallet/txn-list';

export const API_GIFT = '/app/v1/ecommerce/list?page=0&size=100';
export const API_V2_GIFT = '/app/v2/e-commerce/gift-cards';
export const API_V2_GIFT_DETAIL = '/app/v2/e-commerce/gift-card';

export const API_FILE_DOWNLOAD = HOST + '/file/v1/d/';

export const API_OWNED_GIFT = '/app/v2/e-commerce/gift-cards/mine';
export const API_USE_GIFT = '/app/v2/e-commerce/gift-cards/mine/use';
export const API_SEND_GIFT = '/app/v2/e-commerce/gift-cards/mine/send';
export const API_BUY_GIFT = '/app/v1/ecommerce/buy-gc';
export const API_V2_BUY_GIFT = '/app/v2/e-commerce/request/buy-new';
export const API_BUY_CHECK = '/app/v1/ecommerce/buy-gc-status';
export const API_V2_BUY_CHECK = '/app/v2/e-commerce/request';
export const API_FIAT_BALANCE = '/fiat/v1/balance';
export const API_FIAT_BALANCE_CHARGE = '/fiat/v1/balance/charge';

export const API_KYC_FRONT = '/file/v1/user/upload-id-front';
export const API_KYC_BACK = '/file/v1/user/upload-id-back';
export const API_KYC_SELFIE = '/file/v1/user/upload-selfie-with-id';

export const API_WITHDRAW = '/app/v1/wallet/withdraw';
export const API_WITHDRAW_TAN = '/app/v1/wallet/withdraw-request';
export const API_WITHDRAW_HISTORY = '/blockchain/v1/erc20/transfers/';
export const API_WITHDRAW_TOKEN_TO_ADDRESS = '/blockchain/v1/erc20/transfer';

export const API_MY_BANKS = '/app/v2/finance/account/mine';
export const API_ADD_BANKS = '/app/v2/finance/account/add';
export const API_DELETE_BANKS = '/app/v2/finance/account/remove';
export const API_FIAT_BANKS = '/app/v2/finance/banks';
export const API_ALL_FIATS = '/app/v2/finance/fiats';
export const API_FIAT_DISCHARGE = '/fiat/v1/balance/discharge';

export const API_MNT_BALANCE = '/fiat/v1/balance/mine';
export const API_MNT_BALANCE_HISTORY = '/fiat/v1/balance/history';

export const API_NFT_PROFILE_UPDATE = '/app/v2/user/update-nft-profile';

export const API_NFT_UPLOAD_COVER = '/file/v1/user/upload-cover-photo';
export const API_NFT_UPLOAD_PROFILE = '/file/v1/user/upload-profile-photo';

export const API_NFT_LIST = '/app/v2/nft/list';
export const API_NFT_CAT = '/app/v2/nft/public-categories';
export const API_NFT_DETAIL = '/app/v2/nft/';

export const API_NFT_ACTION_COUNT = '/app/v2/nft/action';
export const API_NFT_OWNERS = '/app/v2/nft/';

export const API_NFT_BUY = '/blockchain/v1/erc20/nft/buy';
export const API_NFT_MINE = '/app/v2/nft/mine';
export const API_NFT_HISTORY = '/app/v2/nft/';

export const API_NFT_PUBLIC = '/app/v2/nft/by-nickname';

export const API_GET_CREATER = '/app/v2/user/profile/';
export const API_GET_CREATOR_NFT = '/app/v2/nft/list-by-creator';

export const API_GET_COLLECTION = '/app/v2/nft/list-by-collection';

export const API_BANNER_TOP = '/app/v2/banner/top'

export const TERM = `<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Үйлчилгээний нөхцөл</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>2022.09.04</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>NOMADX.WORLD ҮЙЛЧИЛГЭЭНИЙ НӨХЦӨЛ</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ЭНЭХҮҮ ҮЙЛЧИЛГЭЭНИЙ НӨХЦӨЛ НЬ ТАНД ЦАХИМ ХЭТЭВЧНИЙ ҮЙЛЧИЛГЭЭ ҮЗҮҮЛЭХТЭЙ ХОЛБООТОЙ ТАНЫ ХУВИЙН МЭДЭЭЛЛИЙГ ХҮЛЭЭН АВАХ, ХАДГАЛАХ, УГ НӨХЦӨЛД ЗААСАН ЗОРИЛГООР АШИГЛАХ, БОЛОВСРУУЛАЛТ ХИЙХ ЭРХИЙГ &ldquo;ӨРГӨӨ КОЙН&rdquo; ХХК-Д ОЛГОХ ЭРХ ЗҮЙН ҮР ДАГАВАР БҮХИЙ ХУУЛЬ ЗҮЙН БАРИМТ БИЧИГ ЮМ. &nbsp;ТАНД ТУС ҮЙЛЧИЛГЭЭНИЙ НӨХЦЛҮҮДИЙГ САЙТАР УНШИЖ ТАНИЛЦАЖ, ЗӨВШӨӨРӨХ ЭСЭХЭЭ ШИЙДЭХИЙГ ЗӨВЛӨЖ БАЙНА. ТА &ldquo;НЭВТРЭХ ЭРХ ҮҮСГЭХ&rdquo; ТОВЧЛУУР ДЭЭР ДАРСНААР ЭНЭХҮҮ ҮЙЛЧИЛГЭЭНИЙ НӨХЦЛҮҮДИЙГ ЗӨВШӨӨРЧ, ГАРЫН ҮСЭГ ЗУРСАН ГЭЖ ҮЗНЭ.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Энэхүү үйлчилгээний нөхцөл нь нэрийн дор таны ашиглаж байгаа цахим хэтэвчний вэбсайт болон гар утасны програмд адил хүчин төгөлдөр үйлчилнэ (цаашид &ldquo;Үйлчилгээ&rdquo; гэх).</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ТАВИГДАХ ШААРДЛАГА</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Тус Үйлчилгээ нь Хэрэглэгч цахим өөрийн дансыг удирдан, хяналт тавина. Хэрэглэгч нь нэвтрэх эрх үүсгэж буй өдрийн байдлаар 18 нас хүрсэн байх ёстой бөгөөд ирүүлсэн хувийн мэдээллийн үнэн зөвийг Хэрэглэгч бүрэн хариуцна.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Та цахим хэтэвчний вэбсайтыг ашиглаж эхлэхийн тулд ийм төрлийн арилжаа, гүйлгээг хориглосон эсэхийг сайтар шалгасны дараа &nbsp;хандана уу. Та байршлаасаа хамаараад үйлчилгээг хэсэгчлэн эсвэл бүхэлд нь хэрэглэхийг хязгаарлагдсан байж болно.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ҮЙЛЧИЛГЭЭНИЙ ТОДОРХОЙЛОЛТ</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Бид интернэтэд суурилсан технологитой, цахим мөнгөний зах зээлд (cryptocurrency, эвсэл крипто валют гэх) үүссэн бүтээгдэхүүн буюу цахим мөнгөөр үлчилгээ үзүүлэх платформыг санал болгодог.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Платформоор хангагч нь уг системийн хэвийн ажиллагаа, мэдээллийн аюулгүй байдал халдлагаас урьдчилан сэргийлэх арга хэмжээ, хэрэглэгчидэд зориулсан мэдээ мэдээллийг вэбсайтаар, таны цахим хаягаар дамжуулан мэдэгдэх болно.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгчийг үнэн зөв мэдээллээр хангах чиглэлээр хүчин чармайлт гарган ажиллах бөгөөд вебсайт дах мэдээлэл нь зөвхөн хэрэглэгч таныг бие даан шийдвэр гаргахад дэмжлэг үзүүлэх зорилготой юм.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгч шаардлагатай төхөөрөмжөө бэлтгэн, зардлыг хариуцна.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>1) компьютер ба бусад төхөөрөмж&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>2) интернетийн төлбөр, интернетэд холбогдсон төхөөрөмжийн түрээсийн төлбөр ба үүрэн телефоны төлбөр гэх мэт.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Үйлчилгээг ашиглаж байхдаа та системээс илгээсэн мэдээллүүдтэй &nbsp;тухай бүрт танилцаж байх хэрэгтэй.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Үйлчилгээнийхээ тасралтгүй ба аюулгүй байдлыг ханган ажиллахын тулд өөрсдийн дотоод нөөцийн бүрэн дайчлан ажиллах хэдий ч бидний үйлчилгээг тасалдуулж болох давагдашгүй хүчин зүйл, вирус, хакерын дайралт, системийн хүчин чадалаас давсан тогтворгүй байдал, гуравдагч талын үйлчилгээний саатал, Төрийн байгууллагын шийдвэр, тухайн цахим мөнгөний зах зээлийн товторгүй байдлаас шалтгаалан эрсдэлтэй эөхцөл байдал үүсч болно. &nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Доорх нөхцөл байдал үүссэн үед Nomadx.world нь үйлчилгээг түр хугацаанд зогсоох болон эсвэл дараах арга хэмжээг авах эрхтэй байна. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>А. Зарласны дагуу засвар үйлчилгээ хийх зорилгоор Системийн сул зогсолт хийх</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Б. Үүрэн холбооны операторууд мессежээр илгээж буй кодуудыг дамжуулахгүй байх</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>В. Байгалын гамшиг болох газар хөдлөлт, үер, цахилгааны гэмтэл, дайн, террорист дайралт, нийтийг хамарсан цар тахал ба бусад давагдашгүй хүчин зүйлийн улмаас платформ системд гэмтэл үүсч ингэснээр ажлаа гүйцээж чадахгүй байх&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Г. Хакерийн дайралт, компьютерийн вирусын халдлага ба дайралт, харилцаа холбооны салбарын технологийн тохируулга ба техникийн алдаа, вебсайт сайжруулах ажиллагаа, банкны аюулгүй байдал, засгийн газрын зохицуулалтаас шалгаалан түр хугацаагаар сүлжээ ба үйлчилгээг тасалдуулж, хожимдуулах</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Д. Урьдчилан харах боломжгүй бөгөөд одоогийн байгаа техникийн хүчин зүйлсээр шийдвэрлэх боломжгүй техникийн асуудал үүсэх&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Е. Гуравдагч талын алдаа ба хоцрогдлоос шалтгаалан гуравдагч талд эсвэл хэрэглэгчид үйлчилгээ үзүүлэх боломжгүй болох</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Ё. Системийн доголдол ба сүлжээний доголдол, DDos ба бусад хакерийн халдлага ба бусад гэнэтийн хүчин зүйлээс шалтгаалсан буруу шилжүүлэг, зах зээлийн саатал ба бусад доголдолтой нөхцөл байдал үүсвэл тухайн буруу шилжүүлгийг хүчингүй болгох, тодорхой цаг үеийн бүх шилжүүлгийг буцаах</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Гүйлгээний хэвийн бус байдал: Үйлчилгээг ашиглаж байх явцад гүйлгээ саатах, цахим мөнгөний байршсан системийн сүлжээ болон бусад давагдашгүй хүчин зүйлийн холболтын асуудлаас шалтгаалан Үйлчилгээ тасалдаж болно. Nomadx.world нь хэрэглэгч буруу мэдээлэл оруулах үүнээс шалтгаалж өөрийн данс, мэдээллээ ашиглаж чадахгүй байхтай холбоотой үүссэн ямар нэгэн алдагдал, хохиролд хариуцлага хүлээхгүй.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Үйлчилгээг ашиглаж байгаа хэрэглэгчийн хийсэн шилжүүлгийн бодит үндэслэл ба зорилгыг мэдэх эрхтэй ба хэрэглэгч нь Nomadx.world&ndash;ийн шаардсанаар бодитой, иж бүрэн, үнэн зөв мэдээллээр хангах ёстой. Хэрэв Nomadx.world нь хэрэглэгчийг хуурамч мэдээллийг өгсөн гэж хардах үндэслэл байвал хэрэглэгчийг түр эсхүл бүр мөсөн хязгаарлах эрхтэй болно.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ЦАХИМ МӨНГӨНИЙ ЭРСДЭЛ</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Цахим мөнгөөр арилжаа хийх үед ханшийн зөрүүний алдагдал хүлээх, цахим мөнгийг хадгалах нууцлалаа алдах зэрэг бодит эрсдэлүүд тулгарч болохыг харилцагч ойлгосон байх шаардлагатай юм. Тиймээс та цахим мөнгөөр арилжаа хийх нь таны санхүүгийн нөхцөлд тохиромжтой эсэхийг урьдчилан шийдвэрлэх хэрэгтэй.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Цахим мөнгө зах зээлийн эрэлт хэрэгцээ болон үр ашгаасаа үнэ нь ихээр хэлбэлзэж байдаг.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгч нь өөрийн хийсэн алдаатай худалдан авалтын шилжүүлгээс үүдэлтэй аливаа алдагдлыг хариуцна.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгч нь нууц үгээ мартах ба алдах, нууц үг гэмтэх, хэрэглэгчийн компьютерт, гар утаст өөр хүн халдсан гэх мэт өөрийн алдаатай, буруу ажиллагаанаас үүдэлтэй аливаа алдагдлаа хариуцна.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ЦАХИМ МӨНГӨНИЙ НӨХЦӨЛ</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Бид дараах үе шат, нөхцөл бүхий үйлчилгээг Танд үзүүлнэ. Үүнд:</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Баталгаажуулалт: Харилцагч өөрийн гар утасны дугаар, иргэний үнэмлэхний мэдээллийн хамт бүртгүүлэх бөгөөд сайтад бүртгүүлэн орсны дараа та 3 төрлийн баталгаажуулалтыг хийх шаардлагатай.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Баталгаажуулалт 1: Гар утасныхаа дугаарыг баталгаажуулах. Үүний зорилго нь таны бүртгүүлсэн гар утасны дугаар таны эзэмшилд байгаа эсэхийг баталгаажуулах бөгөөд хэрэв гар утасаа алдсан, гаднаас мессеж хүлээн авах боломжгүй байгаа бол и-мэйл хаягаараа дээрх нууц үгийг авч болно.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Баталгаажуулалт 2: Иргэний үнэмлэхний мэдээллээ батаалгаажуулах: Иргэний үнэмлэхнийхээ зургыг ард, урд талаас нь тус тус нэг нэг хувь авч тус тусад нь хавсарган илгээнэ. Үүнийг бид ажлын 1-2 хоногийн дотор баталгаажуулж, баталгаажсан талаарх хариуг имэйлээр илгээх болно.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Имэйл хаягийн баталгаажуулалтыг таны системд бүртгүүлсэн имэйл хаяг руу нууц кодыг илгээх болно.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Данс үүсгэх &ndash; Та дээрх баталгаажуулалтуудыг хийсний дараа таны нэр дээр цахим данс үүсэх ба та URG койны дансандаа орлогын гүйлгээ хийж эхлэх юм.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Дансандаа орлогын гүйлгээ хийх &ndash; Та дансандаа орлого хийхдээ зөвхөн өөрийн бүртгүүлсэн DAX.MN биржийн данснаасаа хийх ёстой бөгөөд хаягаа үнэн зөв хуулж бичих шаардлагатай.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Данснаасаа зарлагын гүйлгээ хийх &ndash; Та өөрийн данснаасаа DAX.MN биржийн дансруугаа URG койноо татаж авах гүйлгээг хэлнэ.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Цахим мөнгөний зарлагын гүйлгээ хийх хугацаа: Таны арилжаа хийж худалдан авсан цахим мөнгийг шилжүүлэх нь тухайн хүлээн авч байгаа цахим хаягийн орлого хүлээн авах хугацаанаас хамаарах ба энэ хугацаанд үүсч байгаа алдагдал, хохирлыг Өргөө койн ХХК хариуцахгүй болно. Цахим мөнгөний зарлагын гүйлгээ нь харилцан адилгүй хугацаанд шилждэг ба ойролцоогоор 20 минутаас 12 цаг орчим үргэлжилдэг.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ХЭРЭГЛЭГЧИЙН ЭРХ, ҮҮРЭГ</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Та үйлчилгээг ашиглах өөрийн мэдээллээ хамгаалуулах, арилжаа гүйлгээний мэдээллийг авах, оролцохтой холбоотой дараах эрхтэй. Үүнд:</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгчид сайтын холбогдох нөхцлүүдийн дагуу үйлчилгээг найдвартай, тасралтгүй авч үйлчилүүлэх эрхтэй.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгч хувийн мэдээллээ өөрчлөх, гар утасны дугаар, дансны дугаараа өөрчлөх эрхтэй.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгч үзүүлж буй бусад төрлийн үйлчилгээнүүдийг авах эрхтэй.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Үйлчилгээг ашиглахаа зогсоох эрхтэй.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгч нь холбогдох хууль, тогтоомжийг зөрчихгүй байх бөгөөд дараах зорилгоор сайтыг ашиглахыг хориглоно:&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгч дээрх дансаа мөнгө угаах, терроризмыг санхүүжүүлэхтэй зорилгоор болон бусад хууль бус зориулалтаар ашиглахыг хориглох бөгөөд шаардлагатай тохиолдолд мөнгөний хууль ёсны эх үүсвэрийг нотлон харуулна.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгч нь аливаа хууль бус үйл ажиллагаанд оролцох, зохион байгуулах зорилгоор nomadx.world платформыг ашиглахыг хориглоно.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгчээс гаргасан бүхий л төрлийн мэдээлэл нь хүчин төгөлдөр мөрдөгдөж буй хууль, тогтоомж болон сайтын холбогдох нөхцлүүдэд нийцсэн байх бөгөөд аливаа гуравдагч этгээдийн хууль ёсны эрх ашиг, сонирхлыг зөрчөөгүй байна. Хэрэглэгч өөрийн мэдэгдлийн &nbsp;хууль ёсны үр дагаврын хариуцлагыг бүрэн хүлээх ба nomadx.world-д үүссэн хохирлыг нь хэрэглэгчээр төлүүлэх эрхтэй.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ҮЙЛЧИЛГЭЭНИЙ ХУРААМЖ</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>1. Компани холбогдох дүрэм, журмын дагуу үйлчилгээний хураамжийг тогтоох бөгөөд хэрэглэгчдэд зориулж тусгай үйлчилгээний хураамжуудыг тогтоож болно.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;2. Nomadx.world нь хэрэглэгчийн дансны үлдэгдлээс дээр дурдсан үйлчилгээний хураамжийг суутган авах эрхтэй.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ЭРХ ШИЛЖҮҮЛЭХ ТУХАЙ</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хэрэглэгч өөрийн данс, системд нэвтрэх хэрэглэгчийн нэр, нууц үгийг &nbsp; бусдад шилжүүлэх хориотой бөгөөд данс шилжүүлэн ашигласнаас үүдсэн хохирлыг nomadx.world хариуцахгүй.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ХУУЛИЙН ДАГУУ АЖИЛЛАХ&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>nomadx.world нь татвар тооцох суутгах ямар нэг үүрэг хүлээхгүй болно. Мөн Nomadx.world дотоодын хууль сахиулах эрх бүхий байгууллагын тогтоол, шийдвэрээр таны данс болон цахим хөрөнгийг хурааж, хязгаарлаж эсвэл хааж болно.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>МЭДЭЭЛЛИЙН АЮУЛГҮЙ БАЙДЛЫН БОДЛОГО</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Хууль, журам болон зохицуулагч байгууллагын шаардлагуудын нийцлийг хангах,</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Үнэ цэнэ бүхий мэдээлэл болон мэдээллийн хөрөнгийн халдашгүй байдлыг хамгаалах,</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Мэдээлэл болон мэдээллийн хөрөнгийн эргэлзэшгүй байдалд хяналт тавих,</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Мэдээлэл болон мэдээллийн хөрөнгийн тасалдашгүй байдалд хяналт тавих,</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Компанийн нийт ажиллагсад болон гэрээт ажиллагсдын Мэдээллийн аюулгүй байдлыг ханган ажиллахтай холбоотой Мэдлэгийг байнга сайжруулах,</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Мэдээллийн аюулгүй байдлын менежментийн тогтолцоог тасралтгүй сайжруулах,</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Компани хувьцаа эзэмшигч, мерчант байгууллагууд, харилцагч байгууллагууд болон хэрэглэгчдийн мэдээллийг аюулгүй байх нөхцөлөөр хангах</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Компани нь Мэдээллийн аюулгүй байдлын менежментийн тогтолцооны бодлого болон холбогдох баримт бичгүүдийг жил бүр хянаж, түүнд өөрчлөлт орох шаардлагатай тухай бүр шинэчилдэг байна.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>ТУСГААР БАЙДАЛ</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Энэхүү Гэрээний ямар нэг зүйл заалт хүчин төгөлдөр бус болох нь гэрээ бүхэлдээ хүчин төгөлдөр бус болох үндэслэл болохгүй.</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>БУСАД</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>Энэхүү нөхцлүүд нь талуудын хоорондын үүсэх харилцааг зохицуулах бөгөөд энд дурдаагүй нөхцөл, зохицуулалтыг даган мөрдөхгүй.&nbsp;</p>
<p style='margin:0in;line-height:115%;font-size:15px;font-family:"Arial",sans-serif;text-align:justify;'>&nbsp;</p>`;
