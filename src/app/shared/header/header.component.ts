import { Component, OnInit, HostListener, ElementRef, ViewChild, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Subscriber } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { Router, NavigationEnd } from '@angular/router';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})


export class HeaderComponent implements OnInit {

  display: boolean = false;
  activeMenu: any;
  isAuth: boolean = false;
  subscriber: any;
  subscribe: any;
  userSub: any;
  user: any;
  innerWidth: any;
  isMobile = false;
  collapseBtn = false;
  urlLink: any;

  // @ViewChild("scrollTop") private scrollTop: ElementRef;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth <= 861) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
  }

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private messageService: MessageService,
    private router: Router,
    private eventService: EventService,
    private cdr: ChangeDetectorRef
  ) {
    router.events.subscribe((val) => {
      if(val instanceof NavigationEnd){
        this.urlLink = router.url
      }
    })
   }

  ngOnInit(): void {
    this.onResize();
    this.subscriber = this.userService.getAuthenticationState().subscribe(result => { this.isAuth = result });
    this.userSub = this.userService.userInfo.subscribe((usr: any) => { this.user = usr; this.eventService.setUser(usr);});

    this.subscribe = this.eventService.headerMenu.subscribe(res => {
      this.activeMenu = res;
      this.cdr.detectChanges();
    });
  }

  toggleMegaMenu() {
    this.collapseBtn = !this.collapseBtn;
    if (this.collapseBtn) {
      this.collapseBtn = true;
      document.body.classList.add('visible');
    } else {
      this.collapseBtn = false;
      document.body.classList.remove('visible');
    }
  }

  closeMenu() {
    this.collapseBtn = false;
    document.body.classList.remove('visible');
  }

  active(menuIndex: number) {
    this.activeMenu = menuIndex;
    // this.scrollTop.nativeElement.scrollTop = 10;
  }

  showLogout() {
    this.display = true;
  }

  hideDialog() {
    this.display = false;
  }

  logout() {
    this.messageService.add({
      severity: 'success',
      summary: 'Баярлалаа',
      detail: 'Та системээс гарлаа.'
    });
    this.authService.logout();
    this.display = false;
  }
}
