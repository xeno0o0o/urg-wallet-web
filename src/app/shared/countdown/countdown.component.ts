import { Component, Input, OnInit } from '@angular/core';
import { CountdownConfig } from 'ngx-countdown';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit {
  @Input() data: any;

  constructor() { }

  ngOnInit(): void {
  }

  getDateConfig() {
    let conf: CountdownConfig = {
      leftTime: this.getTimeDif(this.data),
      format: this.getTimeDif(this.data) > 86400 ? 'd:HH:mm:ss' : 'HH:mm:ss',
      prettyText: (text) => {
        return text
          .split(':')
          .map((v, index) =>
            this.getTimeDif(this.data) > 86400 ?
              `<span class="__d_cd_item">${v}<div class="_t_label">` +
              (index === 0 ? `Өдөр` : index === 1 ? `Цаг` : index === 2 ? `Мин` : `Сек`)
              + `</div></span>` :
              `<span class="__d_cd_item">${v}<div class="_t_label">` +
              (index === 0 ? `Цаг` : index === 1 ? `Мин` : index === 2 ? `Сек` : `Сек`)
              + `</div></span>`
          )
          .join('');
      },
    }
    return conf;
  }

  getTimeDif(endDate: string) {
    if (endDate === undefined || endDate === null) {
      return 0
    }
    let date = new Date(endDate.replace(/-/g, "/"));
    let diff = date.getTime() - Date.now();
    return Math.floor(diff / 1000);
  }
}
