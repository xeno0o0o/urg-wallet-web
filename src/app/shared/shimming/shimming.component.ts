import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-shimming',
  templateUrl: './shimming.component.html',
  styleUrls: ['./shimming.component.scss']
})
export class ShimmingComponent implements OnInit {
  @Input() data: any;

  constructor() { }

  ngOnInit(): void {
  }

}
