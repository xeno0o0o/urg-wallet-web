import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShimmingComponent } from './shimming.component';

describe('ShimmingComponent', () => {
  let component: ShimmingComponent;
  let fixture: ComponentFixture<ShimmingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShimmingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShimmingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
