import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CountdownComponent } from './countdown/countdown.component';

import { DialogModule } from 'primeng/dialog';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { CountdownModule } from 'ngx-countdown';
import { ShimmingComponent } from './shimming/shimming.component';

const components = [
  HeaderComponent, FooterComponent, CountdownComponent, ShimmingComponent
];

@NgModule({
  declarations: [...components, ShimmingComponent],
  imports: [
    CommonModule,
    RouterModule,
    CountdownModule,
    DialogModule,
    ToastModule
  ],
  exports: [...components],
  providers: [MessageService]
})
export class SharedModule { }
