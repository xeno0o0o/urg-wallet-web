import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
    password: new FormControl('')
  });

  constructor(
    private messageService: MessageService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  login() {
    if (this.loginForm) {
      localStorage.clear();
      this.authService.login(this.loginForm.getRawValue()).subscribe(response => {
        this.router.navigate(['/nft/list']).then();
      }, err => {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'Утас болон нууц үгээ шалгана уу.'
        });
      });
    }
  }
}
