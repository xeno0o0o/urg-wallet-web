import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Meta, Title } from '@angular/platform-browser';
import { IMG_LCL_PATH, IMG_PATH } from 'src/app/shared/uri.constants';
import { EndpointService } from 'src/app/services/endpoint.service';
import { UserService } from 'src/app/services/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { NftBuyModel } from 'src/app/core/model/nft-buy.model';
import { UuidService } from 'src/app/services/uuid.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})

export class DetailComponent implements OnInit {
  IMG_PATH = IMG_PATH;
  IMG_LCL_PATH = IMG_LCL_PATH;
  nft_buy_model: NftBuyModel | undefined;

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
    password: new FormControl('')
  });

  nftBsId: any;
  nftCode: any;

  endDate: string = '2021-12-22 09:00:00';

  likeCount: number = 0;
  viewCount: number = 0;
  shareCount: number = 0;

  selectedFiat: any;
  isCoin = false;

  activeItem = 1;
  subscriber: any;
  userSub: any;

  isLike: boolean = false;
  isShare: boolean = false;
  isAuth: boolean = false;
  isDialogShow: boolean = false;
  isBuyTime: boolean = false;
  isShowPin: boolean = false;

  choosenBalance: any = [];

  cardId: any;
  nft: any = {};
  nftOwners: any = [];
  nftHistorys: any = {};
  isZoom = false;
  userBalance: any;

  collectionPic = '';
  nickname = '';

  isNotYet: boolean = false;
  isNftExpire: boolean = false;

  cardCount = 1;
  user: any;
  usrSmartContract = '';
  password: any;

  constructor(
    private endpointService: EndpointService,
    private messageService: MessageService,
    private activeRoute: ActivatedRoute,
    private route: Router,
    private userService: UserService,
    private meta: Meta,
    private authService: AuthService,
    private uuidService: UuidService,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.activeRoute.params.subscribe(param => {
      this.nftBsId = param.bsId;
      this.nftCode = param.code;
    });
  }

  ngOnInit(): void {
    this.isUserAuth();
    this.getNftDetail();
    this.setMeta(); 
    this.userSub = this.userService.userInfo.subscribe((usr: any) => { this.user = usr; });
  }

  setMeta() {
    this.meta.addTags([
      { name: 'og:url',         content: 'https://nomadx.world/#/nft/detail/' + this.nft.id + '/' + this.nft.code},
      { name: 'og:type',        content: 'article' },
      { name: 'og:title',       content: this.nft.name },
      { name: 'og:description', content: this.nft.description },
      { name: 'og:image',       content: 'https://nomadx.world/#/file/v1/d/2e7f77dd-6607-44a9-9a91-f6103abf4cd8-md.png' },
    ], true);
  }

  onLike() {
    this.isLike = !this.isLike;
    this.likeCount = this.isLike === true ? this.likeCount + 1 : this.likeCount - 1;

    let action = {
      'bs-id': this.nftBsId,
      'code': this.nftCode,
      'type': this.isLike === true ? 'LIKE' : 'UNLIKE'
    }
    this.actionNftCount(action);
  }

  onShare(e: any) {
    this.isShare = !this.isShare;
    this.shareCount = this.isShare ? this.shareCount + 1 : this.shareCount - 1;

    let url = 'https://nomadx.world/#/nft/detail/' + this.nft.id + '/' + this.nft.code;
    e.preventDefault();
    let facebookWindow = window.open(
      'https://www.facebook.com/sharer/sharer.php?u=' +  url,
      'facebook-popup',
      'height=350,width=600'
    );
    if (facebookWindow) {
      if (facebookWindow.focus) {
        facebookWindow.focus();
      }
    }
    let action = {
      'bs-id': this.nftBsId,
      'code': this.nftCode,
      'type': 'SHARE'
    }
    this.actionNftCount(action);
    return false;
    
  }

  actionNftCount(action: any) {
    this.endpointService.getNftActionCount(action).subscribe(res => {
      // console.log(res)
    });
  }

  getNftDetail() {
    this.endpointService.getNftDetail(this.nftBsId, this.nftCode).subscribe(res => {
      this.nft = res;
      this.likeCount = this.nft.likes;
      this.shareCount = this.nft.shares;
      this.viewCount = this.nft.seens;

      this.calculateDate(this.nft.start_date, this.nft.end_date);

      this.isLike = (this.nft.liked_flag === null) ? false : this.nft.liked_flag;
      this.collectionPic = IMG_PATH + this.nft.creator.picture;
      this.nickname = this.nft.creator.nickname;
      this.usrSmartContract = this.nft.smartContract;
      this.getNftOwners();
      this.getNftHistory();
    })
  }

  copyAlert() {
    this.messageService.add({
      severity: 'success',
      summary: 'Амжилттай',
      detail: 'Текстийг амжилттай хуулсан.'
    });
  }

  tabItem(num: any) {
    this.activeItem = num;
  }

  urgImgZoomIn() {
    this.isZoom = !this.isZoom;
  }

  goToCreator() {
    this.route.navigate(['/creator/', this.nft.creator.nickname]);
  }

  getNftOwners() {
    const data = {
      page:0,
      size:10
    }
    this.endpointService.getNftDetailOwners(this.nftBsId, this.nftCode, data).subscribe(res => {
      if (res.status === 0) {
        console.warn(res)
        this.nftOwners = res.owners;
      }
    });
  }

  getNftHistory() {
    const data = {
      page:0,
      size:10
    }
    this.endpointService.getNftHistory(this.nftBsId, this.nftCode, data).subscribe(res => {
      if (res.status === 0) {
        this.nftHistorys = res.histories;
      }
      // console.log('history', res)
    });
  }

  calculateDate(startDate: string, endDate: string) {
    this.isNotYet = (startDate === null || startDate === undefined) ? false : this.getTimeDif(startDate) > 0 ? true : false;
    this.isNftExpire = ( endDate === null || endDate === undefined) ? false : this.getTimeDif(endDate) > 0 ? false : true;
  }

  getTimeDif(endDate: string) {
    let date = new Date(endDate.replace(/-/g, "/"));
    let diff = date.getTime() - Date.now();
    return Math.floor(diff / 1000);
  }

  isUserAuth() {
    this.subscriber = this.userService.getAuthenticationState().subscribe(result => {
      this.isAuth = result;
      if (this.isAuth === true) {
        this.getUserUrgBalance();
      }
    });
  }

  getUserUrgBalance() {
    this.endpointService.getUserUrgBalance().subscribe(res => {
      if (res.status === 0) {
        this.userBalance = res.address ? res.balances : [];
        this.getUserFiat();
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }

  getUserFiat() {
    this.endpointService.getMNTbalance().subscribe(res => {
      if (res.status === 0) {
        const userFiat = res.balances;
        userFiat.forEach((element: any) => {
          if (element !== null || element !== undefined) {
            this.userBalance.push(element); 
          }
        });
      }
    });
  }

  selectFiat(fiat: any) {
    this.selectedFiat = fiat;
    if (this.isAuth) {
      this.choosenBalance = this.userBalance.find((item: any) =>
        item.code === this.selectedFiat.symbol
      );
      if (this.choosenBalance.code === 'MNT') {
        this.isCoin = true;
      } else {
        this.isCoin = false;
      }
    }
  }
  
  showDialog() {
    if (this.isNftExpire) {
      this.messageService.add({
        severity: 'info',
        summary: 'Амжилтгүй',
        detail: 'Уучлаарай тус бэлгийн хугацаа дууссан байна.'
      });
    } else if (this.selectedFiat === null || this.selectedFiat === undefined) {
      this.messageService.add({
        severity: 'info',
        summary: 'Мэдэгдэл',
        detail: 'Та эхлээд төлбөр төлөх боломжийг сонгоно уу.'
      });
    } else {
      this.isDialogShow = true;
    }
  }

  buyNft() {
    if (this.user) {
      if (this.user.statusCode !== 2) {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: 'Таны хэрэглэгчийн мэдээлэл баталгаажаагүй байна'
        });
        this.document.body.classList.remove('p-overflow-hidden');
        return;
      }
    } else {
      this.userService.setUserInfo();
      this.userService.userInfo.subscribe((usr: any) => { this.user = usr; });
    }
    let currentUUID;
    if (localStorage.getItem('nInq') === null || localStorage.getItem('nInq') === undefined) {
      currentUUID = this.uuidService.getUUID();
      localStorage.setItem('nInq', currentUUID);
      let data = {
        "nftCode": this.nft.code,
        "count": this.cardCount,
        "pinCode": this.password,
        "priceId": this.selectedFiat.id,
        "inquireId": currentUUID
    }
      
      this.isBuyTime = true;
      this.endpointService.buyNft(data).subscribe(res => {
        if (res.status === 0) {
          this.messageService.add({
            severity: 'info',
            summary: 'Мэдэгдэл',
            detail: res.msg
          });
          this.isBuyTime = false;
          this.isDialogShow = false;
          this.document.body.classList.remove('p-overflow-hidden');
          localStorage.removeItem('nInq');
        } else {
          this.isBuyTime = false;
          this.isDialogShow = false;
          this.document.body.classList.remove('p-overflow-hidden');
          localStorage.removeItem('nInq');
          this.messageService.add({
            severity: 'info',
            summary: 'Мэдэгдэл',
            detail: res.msg
          });
        }
      });
    } else {
      this.isBuyTime = true;
      this.isDialogShow = true;
    }
  }

  showPinSection() {
    this.isShowPin = !this.isShowPin;
  }
  
  hideDialog() {
    this.isDialogShow = false;
    this.isShowPin = false;
    this.document.body.classList.remove('p-overflow-hidden');
  }

  login() {
    if (this.loginForm) {
      localStorage.clear();
      this.authService.login(this.loginForm.getRawValue()).subscribe(response => {
        this.route.navigate(['/home']).then();
      }, err => {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'Утас болон нууц үгээ шалгана уу.'
        });
      });
    }
  }

  
}
