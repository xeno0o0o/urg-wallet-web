import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';
import { IMG_PATH } from 'src/app/shared/uri.constants';


@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
})
export class MarketComponent implements OnInit {

  IMG_PATH = IMG_PATH;

  selectedIndex: number = 0;
  marketType: any;
  cities: any = [];
  selectedCity: any;
  totalData = 0;
  
  collectedList: any = [];
  nftCats: any = [];
  nftList: any = [];
  cats: any = [];
  nfts: any = [];
  catPage = {
    'category-ids': 0,
    'page': 0,
    'size': 12
  }
  nftPage = {
    'category-ids': 0,
    'page': 0,
    'size': 12
  }

  constructor(
    private eventService: EventService,
    private route: ActivatedRoute,
    private endpointService: EndpointService
  ) {
    this.marketType = this.route.snapshot.paramMap.get('type');

    this.cities = [
      { name: 'New York', code: 'NY' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' }
    ];
  }

  ngOnInit(): void {
    this.getNftCats();
    this.eventService.headerMenuChange(4);
  }
  
  getNftCats() {
    this.endpointService.getNftCat().subscribe(res=> {
      if (res.status === 0) {
        if (res.categories.length > 0) {
          this.nftCats = [];
          this.nftList = [];
          res.categories.forEach((element: any) => {
            if (element.activeFlag) {
              this.nftCats.push(element);
            }
          });
          this.nftCats.forEach((element: any) => {
            this.catPage['category-ids'] = element.id;
            this.endpointService.getNftList(this.catPage).subscribe(res => {
              if (res.status === 0) {
                element.nfts = res.nfts;
                this.nftList.push(...res.nfts);
                this.nftList = this.nftList.filter((v: any,i: any,a: any)=>a.findIndex((t: any)=>(t.code===v.code))===i);
              }
            });
          });
        }
      }
    })
  }

  getNfts(cat: any) {
    this.nftPage['category-ids'] = cat;
    this.endpointService.getNftList(this.nftPage).subscribe(res => {
      if (res.status === 0) {
        this.totalData = res.total;
        this.nftList = res.nfts;
        this.eventService.setNFTList(this.nfts);
      }
    });
  }

  paginate(event: any) {
    this.nftPage.page = event.page;
    if (this.selectedIndex === 0) {
      this.getNftCats();
    } else {
      this.getNfts(this.selectedIndex);
    }
  }

  selectTab(item: any) {
    if (item === 'all') {
      this.selectedIndex = 0;
      this.marketType = 'all';
      this.getNftCats();
    } else {
      this.selectedIndex = item.id;
      this.marketType = item.code;
      this.getNfts(item.id)
    }
  }

  
  calculateDate(startDate: string, endDate: string) {
    let isNotYet = this.getTimeDif(startDate) > 0 ? true : false;
    let isNftExpire = this.getTimeDif(endDate) > 0 ? false : true;

    
    if (isNotYet === false && isNftExpire === false) {
      return 'active'
    }  
    if (isNotYet === true && isNftExpire === false) {
      return 'soon'
    }
    if (isNotYet === false && isNftExpire === true) {
      return 'closed'
    }
    return 'soon';
  }

  getTimeDif(endDate: string) {
    let date = new Date(endDate.replace(/-/g, "/"));
    let diff = date.getTime() - Date.now();
    return Math.floor(diff / 1000);
  }
}
