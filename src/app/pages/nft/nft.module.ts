import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { ClipboardModule } from 'ngx-clipboard';
import { MessageService } from 'primeng/api';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { FileUploadModule } from 'primeng/fileupload';
import { PaginatorModule } from 'primeng/paginator';
import { NftComponent } from './nft.component';
import { MainComponent } from './main/main.component';
import { DetailComponent } from './detail/detail.component';

import { NftResolver } from 'src/app/services/Resolver/nft.resolver';
import { SharedModule } from 'src/app/shared/shared.module';
import { MarketComponent } from './market/market.component';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: '',
    component: NftComponent,
    children: [
      { path: 'list', component: MainComponent },
      { path: 'detail/:bsId/:code', component: DetailComponent },
      { path: 'market/:type', component: MarketComponent }
    ]
  }
];

@NgModule({
  declarations: [
    NftComponent,
    MainComponent,
    DetailComponent,
    MarketComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ProgressSpinnerModule,
    SharedModule,
    DialogModule,
    ToastModule,
    SwiperModule,
    ClipboardModule,
    FileUploadModule,
    HttpClientModule,
    PaginatorModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  providers: [MessageService]
})
export class NftModule { }
