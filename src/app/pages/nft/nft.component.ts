import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-nft',
  templateUrl: './nft.component.html',
  styleUrls: ['./nft.component.scss']
})
export class NftComponent implements OnInit {

  nftCats: any = [];
  cats: any = [];
  nfts: any = [];

  page = {
    'category-ids': '1,2,3',
    'page': 0,
    'size': 10
  }

  constructor(
    private eventService: EventService,
    private endpointService: EndpointService
  ) { }

  ngOnInit(): void { }

  getNftCats() {
    this.endpointService.getNftCat().subscribe(res=> {
      if (res.status === 0) {
        if (res.categories.length > 0) {
          res.categories.forEach((element: any) => {
            if (element.activeFlag) {
              this.nftCats.push(element)
              this.cats.push(element.id);
            }
          });
          this.page['category-ids'] = this.cats;
          this.getNfts();
        }
      }
    })
  }

  getNfts() {
    this.endpointService.getNftList(this.page).subscribe(res => {
      if (res.status === 0) {
        this.nfts = res.nfts;
        this.eventService.setNFTList(this.nfts);
      }
    });
  }

}
