import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, HostListener } from '@angular/core';
import { EventService } from 'src/app/services/event.service';
import {
  SwiperComponent, SwiperDirective, SwiperConfigInterface,
  SwiperScrollbarInterface, SwiperPaginationInterface
} from 'ngx-swiper-wrapper';
import { EndpointService } from 'src/app/services/endpoint.service';
import { IMG_PATH } from 'src/app/shared/uri.constants';

declare var $: any;
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {
  @ViewChild('currentWrap', { static: false }) currentWrap: ElementRef | undefined;

  collectedList: any = [];
  nftCats: any = [];
  cats: any = [];
  nfts: any = [];

  IMG_PATH = IMG_PATH;

  markStyle: any;
  urgDaxVal: any;

  banners: any = [];

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.resizeSlide()
  }

  public config: SwiperConfigInterface = {
    
  };

  constructor(
    private eventService: EventService,
    private cdr: ChangeDetectorRef,
    private endpointService: EndpointService
  ) { }

  ngOnInit(): void {
    this.getNftCats();
    this.getDaxVal();
    this.getBanner();
    setTimeout(() => {
      this.config = {
        slidesPerView: 1,
        navigation: true,
        autoplay: {
          delay: 3000
        }
      }
    }, 1000);
    this.eventService.headerMenuChange(1);
  }

  ngAfterViewInit() {
    this.resizeSlide();
    this.cdr.detectChanges();
  }

  
  getNftCats() {
    this.endpointService.getNftCat().subscribe(res=> {
      if (res.status === 0) {
        if (res.categories.length > 0) {
          res.categories.forEach((element: any) => {
            if (element.activeFlag) {
              this.nftCats.push(element);
            }
          });

          this.nftCats.forEach((element: any) => {
            const data = {
              'category-ids': element.id,
              'page': 0,
              'size': 10
            }
            this.endpointService.getNftList(data).subscribe(res => {
              if (res.status === 0) {
                element.nfts = res.nfts;
              }
            });
          });
          // console.log(this.nftCats)
        }
      }
    })
  }

  getNfts(cat: any) {
    const data = {
      'category-ids': cat,
      'page': 0,
      'size': 10
    }
    this.endpointService.getNftList(data).subscribe(res => {
      if (res.status === 0) {
        this.nfts = res.nfts;
        this.eventService.setNFTList(this.nfts);
      }
    });
  }

  resizeSlide() {
    let resize = ((this.currentWrap?.nativeElement.offsetWidth * 2.78) / 10);
    this.markStyle = resize;
  }

  getDaxVal() {
    const data = {
      "query": "query ActivePairs {\n  sys_pair(where: {is_active: {_eq: true}}, order_by: {id: asc}) {\n    id\n    symbol\n    base_max_size\n    base_min_size\n    base_tick_size\n    quote_max_size\n    quote_min_size\n    quote_tick_size\n    baseAsset {\n      id\n      code\n      name\n      scale\n      is_crypto\n      __typename\n    }\n    quoteAsset {\n      id\n      code\n      name\n      scale\n      __typename\n    }\n    price {\n      last_price\n      last_price_dt\n      __typename\n    }\n    stats24 {\n      high\n      low\n      change24h\n      vol\n      __typename\n    }\n    __typename\n  }\n}\n"
    }
    this.endpointService.getURGbyDax(data).subscribe((res: any) => {
      if (res.data) {
        const urgData = res.data.sys_pair.find((el: any) => el.symbol === 'URGMNT');
        this.urgDaxVal = urgData.price.last_price / 100;
      }
    });
  }

  calculateDate(startDate: string, endDate: string) {
    let isNotYet = this.getTimeDif(startDate) > 0 ? true : false;
    let isNftExpire = this.getTimeDif(endDate) > 0 ? false : true;

    
    if (isNotYet === false && isNftExpire === false) {
      return 'active'
    }  
    if (isNotYet === true && isNftExpire === false) {
      return 'soon'
    }
    if (isNotYet === false && isNftExpire === true) {
      return 'closed'
    }
    return 'soon';
  }

  getTimeDif(endDate: string) { 
    let date = new Date(endDate.replace(/-/g, "/"));
    let diff = date.getTime() - Date.now();
    return Math.floor(diff / 1000);
  }

  getBanner() {
    this.endpointService.getBannerTop().subscribe(res => {
      if (res.status === 0) {
        this.banners = res.banners.filter((item: any) => item.activeFlag === true);
      }
      console.log('banners', this.banners);
    });
  }

}
