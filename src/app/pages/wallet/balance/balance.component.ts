import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { BalanceService } from 'src/app/services/balance.service';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html'
})

export class BalanceComponent implements OnInit {

  isShow = false;
  userBalance: any = [];
  balanceToMNT: number = 0;
  balanceToURG: number = 0;

  daxValues: any = [];
  daxUrgVal: any;
  usrMnt: any;

  userFiat: any;


  constructor(
    private endpointService: EndpointService,
    private balanceService: BalanceService,
    private event: EventService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getUserFiat();
    this.getCurrentValues();
    this.getBalance();
    this.event.walletMenuChange(1);
  }

  totalBalance() {
    this.isShow = !this.isShow
  }

  getBalance() {
    this.endpointService.getUserUrgBalance().subscribe(res => {
      if (res.status === 0) {
        this.userBalance = res.balances;
        this.convertMNT(this.userBalance);
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }

  getUserFiat() {
    this.endpointService.getMNTbalance().subscribe(res => {
      if (res.status === 0) {
        this.userFiat = res.balances;
      }
    });
  }

  getCurrentValues() {
    this.balanceService.getURGbyDax().subscribe((res: any) => {
      if (res.data) {
        this.daxValues = res;
        const urgData = res.data.sys_pair.find((el: any) => el.symbol === 'URGMNT');
        this.daxUrgVal = urgData.price.last_price / 100;
      }
    });
  }

  convertMNT(balances: any) {
    let coinBalances: any[] = [];
    balances.forEach((element: any) => {
      if (element.code === 'URGT') {
        element.code = 'URG'
      }
      if (element.code === 'GAHC') {
        element.code = 'URG'
      }
      this.daxValues.data.sys_pair.find((el: any) => {
        if (el.baseAsset.code === element.code && el.quoteAsset.code === 'MNT') {
          coinBalances.push((el.price.last_price / 100) * element.available)
        }
      })
      if (element.symbol === 'URGT') {
        element.code = 'URGT'
      }
      if (element.symbol === 'GAHC') {
        element.code = 'GAHC'
      }
    });
    if (coinBalances.length > 0) {
      this.balanceToMNT = coinBalances.reduce(function (acc: any, cur: any) {
        return acc + cur;
      });
    } else {
      this.balanceToMNT = 0;
    }
    this.userFiat.forEach((element: any) => {
      this.balanceToMNT = this.balanceToMNT + element.balance;
    });
    this.balanceToURG = this.balanceToMNT / this.daxUrgVal;
  }

  currentIcon(code: any) {
    const localPath = '../../../../assets/images/';
    if (code === 'MNT') {
      return localPath + 'mnt.svg'
    } else {
      return localPath + 'urg_coin.webp'
    }
  }
}
