import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';
import { BLOCK_DEC, IMG_PATH } from 'src/app/shared/uri.constants';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnDestroy {
  sendGiftForm: FormGroup = new FormGroup({
    phone: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(8),
      Validators.pattern('(?=.*[0-9]).{8,}'),
    ]),
  });
  ownedGiftData: any = [];
  ownedLotteryData: any = [];
  blockDec: number = BLOCK_DEC;
  IMG_PATH: string = IMG_PATH;
  selectedGift: any = [];
  isShowGiftSection: boolean = false;
  isDialogShow: boolean = false;
  promoCode = ' ';
  hasPin: any;
  promoPin: any;

  nextPage: any;
  isShowNextPage: boolean = true;
  totalData = 0;

  page = {
    'page': 0,
    'size': 10
  }

  constructor(
    private messageService: MessageService,
    private endpointService: EndpointService,
    private event: EventService
  ) { }


  ngOnDestroy(): void {
    localStorage.removeItem('crdPage');
    localStorage.removeItem('crdSize');
  }

  ngOnInit(): void {
    this.getOwnedGift();
    this.event.walletMenuChange(5);
  }

  getOwnedGift() {
    this.endpointService.getOwnedGift(this.page).subscribe(res => {
      if (res.status === 0) {
        this.ownedGiftData = res.mines;
        this.totalData = res.total;
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: res.msg
        });
      }
    });
  }


  useGift(giftCard: any) {
    let data = {
      "gift-card-id": giftCard.giftCardId,
      "gift-card-item-id": giftCard.giftCardItemId
    }
    this.endpointService.useGiftCard(data).subscribe(res => {
      if (res) {
        if (res.status === 0) {
          this.promoCode = res.barCode;
          this.hasPin = res.hasPin;
          if (this.hasPin) {
            this.promoPin = res.pin;
          }
          this.showUseGiftDialog();
          this.messageService.add({
            severity: 'info',
            summary: 'Мэдэгдэл',
            detail: res.msg
          });
        }
      }
    });
  }
  sendGiftCard() {
    let data = {
      'gift-card-id': this.selectedGift.giftCardId,
      'gift-card-item-id': this.selectedGift.giftCardItemId,
      'phone-no': this.sendGiftForm.controls['phone'].value
    }
    this.endpointService.sendGiftCard(data).subscribe(res => {
      if (res) {
        this.messageService.add({
          severity: res.status === 1 ? 'info' : 'success',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
        if (res.status === 0) {
          this.isShowGiftSection = !this.isShowGiftSection;
        }
      }
    })
  }

  showGiftSection(gift: any) {
    this.isShowGiftSection = true;
    this.selectedGift = gift;
  }

  backToGiftList() {
    this.isShowGiftSection = false;
    if (localStorage.getItem('crdPage') !== null || localStorage.getItem('crdPage') !== undefined) {
      const page = localStorage.getItem('crdPage');
      this.page.page = (page === null) ? 0 : +page;
      this.getOwnedGift();
    }
  }
  showUseGiftDialog() {
    this.isDialogShow = true;
  }
  hideUseGiftDialog() {
    this.getOwnedGift();
    this.isDialogShow = false;
    document.body.classList.remove('p-overflow-hidden');
  }

  paginate(event: any) {
    this.page = {
      'page': event.page,
      'size': 10
    };

    localStorage.setItem('crdPage', this.page.page.toString());
    localStorage.setItem('crdSize', this.page.size.toString());

    this.getOwnedGift();
  }
}
