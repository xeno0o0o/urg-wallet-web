import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-buycoin',
  templateUrl: './buycoin.component.html',
  styleUrls: ['./buycoin.component.scss']
})
export class BuycoinComponent implements OnInit {

  displayDialog = false;
  userPassword = '';

  banks: any;
  selectedBank: any;
  selectedCity = [];
  myBanks = [];
  fiatAmount: any;
  urgAmount: any;
  urgDaxVal = 0;
  getAmount = 0;

  constructor(
    private endpointService: EndpointService,
    private messageService: MessageService,
    private event: EventService
  ) { }

  ngOnInit(): void {
    this.getURGbyDax();
    this.getMyBankAcc();
    this.getBanks();
    this.event.walletMenuChange(6);
  }

  mntCount(e: any) {
    this.urgAmount = this.fiatAmount / this.urgDaxVal;
  }

  urgCount(e: any) {
    this.fiatAmount = this.urgAmount * this.urgDaxVal;
  }

  showFiatDialog() {
    this.displayDialog = !this.displayDialog;
  }

  sendBuyReq() {

  }

  selectBank(item: any) {
    this.selectedBank = item;
  }

  getMyBankAcc() {
    this.endpointService.getMyBankList().subscribe(res => {
      if (res.status === 0) {
        this.myBanks = res.accounts;
        if (this.myBanks.length > 0) {
          this.selectedBank = this.myBanks[0];
        }
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'Mine Bank хүсэлтэд алдаа гарлаа.'
        });
      }
    });
  }

  getBanks() {
    this.endpointService.getBanks().subscribe(res => {
      if (res.status === 0) {
        this.banks = res.banks;
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'bank хүсэлтэд алдаа гарлаа.'
        });
      }
    });
  }

  getURGbyDax() {
    const data = {
      "query": "query ActivePairs {\n  sys_pair(where: {is_active: {_eq: true}}, order_by: {id: asc}) {\n    id\n    symbol\n    base_max_size\n    base_min_size\n    base_tick_size\n    quote_max_size\n    quote_min_size\n    quote_tick_size\n    baseAsset {\n      id\n      code\n      name\n      scale\n      is_crypto\n      __typename\n    }\n    quoteAsset {\n      id\n      code\n      name\n      scale\n      __typename\n    }\n    price {\n      last_price\n      last_price_dt\n      __typename\n    }\n    stats24 {\n      high\n      low\n      change24h\n      vol\n      __typename\n    }\n    __typename\n  }\n}\n"
    }
    this.endpointService.getURGbyDax(data).subscribe((res: any) => {
      // .log(res.data.sys_pair)
      if (res.data) {
        const urgData = res.data.sys_pair.find((el: any) => el.symbol === 'URGMNT');
        this.urgDaxVal = urgData.price.last_price / 100;  
      }
    }, error => {
      this.urgDaxVal = 0;
    });
  }

}
