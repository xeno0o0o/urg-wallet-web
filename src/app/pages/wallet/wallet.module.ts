import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { WalletComponent } from './wallet.component';
import { IncomeComponent } from './income/income.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { HistoryComponent } from './history/history.component';

import { QRCodeModule } from 'angularx-qrcode';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { TabMenuModule } from 'primeng/tabmenu';
import { NgOtpInputModule } from 'ng-otp-input';
import { DialogModule } from 'primeng/dialog';
import { BalanceComponent } from './balance/balance.component';
import { DropdownModule } from 'primeng/dropdown';
import { PaginatorModule } from 'primeng/paginator';
import { CardComponent } from './card/card.component';
import { BuycoinComponent } from './buycoin/buycoin.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'balance',
    pathMatch: 'full',
  },
  {
    path: '',
    component: WalletComponent,
    children: [
      {
        path: 'card',
        component: CardComponent,
      },
      {
        path: 'balance',
        component: BalanceComponent,
      },
      {
        path: 'income',
        component: IncomeComponent,
      },
      {
        path: 'income/:coinCode',
        component: IncomeComponent,
      },
      {
        path: 'withdraw',
        component: WithdrawComponent,
      },
      {
        path: 'withdraw/:coinCode',
        component: WithdrawComponent,
      },
      {
        path: 'history',
        component: HistoryComponent,
      },
      // {
      //   path: 'buy',
      //   component: BuycoinComponent,
      // }
    ]
  }
];

@NgModule({
  declarations: [
    WalletComponent,
    IncomeComponent,
    WithdrawComponent,
    HistoryComponent,
    BalanceComponent,
    CardComponent,
    BuycoinComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    QRCodeModule,
    ClipboardModule,
    ToastModule,
    TabMenuModule,
    NgOtpInputModule,
    PaginatorModule,
    DialogModule,
    DropdownModule,
    RouterModule.forChild(routes),
  ],

  exports: [RouterModule],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'en-EN'
    },
    MessageService
  ]
})
export class WalletModule { }
