import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit, OnDestroy {
  active: any;
  subscribe: any;
  menu: any = [
    'balance', 'income', 'withdraw', 'history'
  ]
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private eventService: EventService,
    private cdr: ChangeDetectorRef
  ) { }
  ngOnDestroy(): void {
    if (this.subscribe) { 
      this.subscribe.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.subscribe = this.eventService.walletMenu.subscribe(res => {
      this.active = res;
      this.cdr.detectChanges();
    });
    this.eventService.headerMenuChange(3);
  }

  activeTab(tabIndex: number) {
    this.active = tabIndex;
  }
}
