import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { BLOCK_DEC } from 'src/app/shared/uri.constants';
import { ToastModule } from 'primeng/toast';
import { UuidService } from 'src/app/services/uuid.service';
import { ActivatedRoute } from '@angular/router';
import { EventService } from 'src/app/services/event.service';

declare var angular: any;
interface City {
  name: string,
  code: string
}

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent implements OnInit {
  @ViewChild("ngOtpInput", { static: false }) ngOtpInput: any;

  cities: City[];

  selectedCity: City | undefined;

  config = {
    allowNumbersOnly: true,
    length: 4,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: "*",
    inputStyles: {
      width: "50px",
      height: "50px",
    },
  };
  otp: any;
  withdrawHistory: any = [];
  userBalanceInfo: any = [];
  userCoins: any = [];
  userFiat: any = [];
  selectedCoin: any;
  selectedCoinBalance: number = 0;
  isAddressCreated: boolean = false;
  isShowMNT: boolean = false;
  blockDec: number = BLOCK_DEC;
  testPrice: any;
  minWithdraw: number = 50000;
  userPassword: any;
  fee: number = 0;
  userHasCurrency: boolean = false;
  displayDialog: boolean = false;
  fiatDialog = false;
  withdrawForm: FormGroup = new FormGroup({
    address: new FormControl('', [Validators.required]),
    withdraw: new FormControl('', [Validators.required])
  });

  fiatForm: FormGroup = new FormGroup({
    inquireId: new FormControl('', [Validators.required]),
    pinCode: new FormControl('', [Validators.required]),
    dischargeAmount: new FormControl('', [Validators.required]),
    bankAccountId: new FormControl('', [Validators.required]),
  });

  myBanks: any = [];
  banks: any = [];
  selectedBank: any = [];

  page = {
    'type': 'out',
    'page': 0,
    'size': 10
  };
  totalData = 0;

  constructor(
    private endpointService: EndpointService,
    private messageService: MessageService,
    private uuidService: UuidService,
    private route: ActivatedRoute,
    private event: EventService
  ) { 
    this.cities = [
      {name: 'URG', code: 'URG'},
      {name: 'MNT', code: 'MNT'},
    ];
    this.selectedCity = this.cities[0];
  }

  ngOnInit(): void {
    this.getUserUrgBalance();
    this.getMyBankAcc();
    this.getBanks();
    this.event.walletMenuChange(3);
  }
  

  getUserUrgBalance() {
    this.userBalanceInfo = [];
    this.endpointService.getUserUrgBalance().subscribe(res => {
      if (res.status === 0) {
        this.userBalanceInfo = res;
        this.userCoins = res.balances;
        this.getUserFiat();
        this.isAddressCreated = res.address;
        this.getWithdrawHistory();
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }

  selectDefualtCoin(balances: any) {
    let coinCode = this.route.snapshot.paramMap.get('coinCode');
    if (coinCode === null || coinCode === undefined) {
      this.selectedCoin = this.userCoins != null ? balances[0] : '';
      this.selectedCoinBalance = this.userCoins != null ? balances[0].available : 0;
    } else {
      let currentCoin;
      this.isShowMNT = coinCode === 'MNT' ? true : false;
      currentCoin = balances.find((coin: any) => coin.code === coinCode);
      this.selectedCity = currentCoin;
      this.selectedCoin = currentCoin;
      this.selectedCoinBalance = coinCode === 'MNT' ? currentCoin.balance : currentCoin.available;
    }
  }

  selectBank(item: any) {
    this.selectedBank = item;
  }

  copyAlert() {
    this.messageService.add({
      severity: 'success',
      summary: 'Амжилттай',
      detail: 'Текстийг амжилттай хуулсан.'
    });
  }
  
  getUserFiat() {
    this.endpointService.getMNTbalance().subscribe(res => {
      if (res.status === 0) {
        this.userFiat = res.balances;
        this.userFiat.forEach((element: any) => {
          if (element !== null || element !== undefined) {
            this.userCoins.push(element); 
          }
          this.selectDefualtCoin(this.userCoins);
        });
      }
    });
  }

  sendWithDraw() {
    const data = {
      "toAddress": this.withdrawForm.controls['address'].value,
      "transferAmount": +this.withdrawForm.controls['withdraw'].value,
      "tanCode": this.otp,
      "inquireId": this.uuidService.getUUID()
    }
    this.endpointService.sendWithDraw(data).subscribe(res => {
      if (res.status === 0) {
        this.messageService.add({
          severity: 'success',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      } else if (res.status === 1) {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }

  showFiatDialog() {
    this.fiatDialog = true;
  }

  showOtpDialog() {
    this.otp = '';
    this.displayDialog = true;
  }

  onOtpChange(otp: string) {
    this.otp = otp;
    if (this.otp.length === 6) {
      setTimeout(() => {
        this.sendWithDraw();
      }, 1000);
    }
  }

  getWithdrawHistory() {
    if (this.selectedCoin.code === 'MNT') {

    } else {
      this.endpointService.getWithdrawHistory(this.selectedCoin.code, this.page).subscribe(res => {
        if (res.status === 0) {
          this.withdrawHistory = res.transfers;
          this.totalData = res.total;
        } else {
          this.messageService.add({
            severity: 'info',
            summary: 'Мэдэгдэл',
            detail: res.msg
          });
        }
      });
    }
  }

  paginate(event: any) {
    this.page = {
      'type': 'out',
      'page': event.page,
      'size': 10
    };

    this.getWithdrawHistory();
  }

  selectCoin(coin: any) {
    this.selectedCoin = coin;
    this.isShowMNT = coin.code === 'MNT' ? true : false;
    this.selectedCoinBalance = coin.code === 'MNT' ? coin.balance : coin.available;
    this.getWithdrawHistory();
    if (this.isShowMNT) {
      this.selectedCoinBalance = coin.balance;
    }
  }

  checkTransDisable() {
    return (this.withdrawForm.controls['address'].valid && +this.withdrawForm.controls['withdraw'].value > 50000 && +this.withdrawForm.controls['withdraw'].value <= this.selectedCoin.available) ? false : true;
  }

  checkFiatDisable() {
    return (+this.fiatForm.controls['dischargeAmount'].value > 0 && this.selectedBank !== undefined) ? false : true;
  }

  currentIcon(code: any) {
    const localPath = '../../../../assets/images/';
    if (code === 'MNT') {
      return localPath + 'mnt.svg'
    } else {
      return localPath + 'urg_coin.webp'
    }
  }

  getMyBankAcc() {
    this.endpointService.getMyBankList().subscribe(res => {
      if (res.status === 0) {
        this.myBanks = res.accounts;
        if (this.myBanks.length > 0) {
          this.selectedBank = this.myBanks[0];
        }
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'Mine Bank хүсэлтэд алдаа гарлаа.'
        });
      }
    });
  }

  getBanks() {
    this.endpointService.getBanks().subscribe(res => {
      if (res.status === 0) {
        this.banks = res.banks;
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'bank хүсэлтэд алдаа гарлаа.'
        });
      }
    });
  }

  sendTransfer() {
    this.fee = (+this.withdrawForm.controls['withdraw'].value * this.selectedCoin.withdrawPercentage) / 100;
    const data = {
      "to": this.withdrawForm.controls['address'].value,
      "amount": +this.withdrawForm.controls['withdraw'].value,
      "pinCode": this.userPassword,
      "tokenCode": this.selectedCoin.code,
      "bsNetworkId": this.selectedCoin.bsNetworkId,
      "inquireId": this.uuidService.getUUID()
    }
    this.endpointService.transferTokenToAddress(data).subscribe(res => {
      this.displayDialog = !this.displayDialog;
      this.userPassword = '';
      this.withdrawForm.reset();
      this.getUserUrgBalance();
      this.messageService.add({
        severity: 'info',
        summary: 'Мэдэгдэл',
        detail: res.msg
      });
    });
  }

  sendFiatTransfer() {
    const selVal = this.selectedCoin;
    let bank = this.banks.find((item: any) => item.code === this.selectedBank.bankCode);
    const data = {
      "bankAccountId": this.selectedBank.id,
      "dischargeAmount": +this.fiatForm.controls['dischargeAmount'].value,
      "pinCode": this.userPassword,
      "inquireId": this.uuidService.getUUID()
    }

    this.endpointService.dischargeBalance(data).subscribe(res => {
      this.fiatDialog = !this.fiatDialog;
      this.userPassword = '';
      this.fiatForm.reset();
      this.getUserUrgBalance();
      
      setTimeout(() => {
        this.selectedCoin = this.userCoins.find((x: any) => x.code === selVal.code);
        this.selectedCoinBalance = this.selectedCoin.balance;
      }, 500);

      this.messageService.add({
        severity: 'info',
        summary: 'Мэдэгдэл',
        detail: res.msg
      });
    }, error => {
      this.fiatDialog = !this.fiatDialog;
      this.userPassword = '';
      this.fiatForm.reset();
      this.messageService.add({
        severity: 'info',
        summary: 'Мэдэгдэл',
        detail: error.message
      });
    })
  }
}
