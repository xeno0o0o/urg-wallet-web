import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';
import { BLOCK_DEC } from 'src/app/shared/uri.constants';

interface City {
  name: string,
  code: string
}

@Component({
  selector: 'app-income',
  templateUrl: './income.component.html',
  styleUrls: ['./income.component.scss']
})
export class IncomeComponent implements OnInit {

  cities: City[];

  selectedCity: City | undefined;

  userBalanceInfo: any = [];
  userCoins: any;
  userFiat: any = [];
  userFiatHistory: any = [];
  selectedCoin: any = null;
  selectedCoinBalance: number = 0;
  walletInsList: any = [];

  historySymbol: any;

  isShowMNT: boolean = false;
  banks: any=[];

  isAddressCreated: boolean = false;
  blockDec: number = BLOCK_DEC;

  page = {
    'type': 'in',
    'page': 0,
    'size': 10
  };
  totalData = 0;
  totalMNT = 0;

  constructor(
    private endpointService: EndpointService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private event: EventService
  ) { 
    this.cities = [
      {name: 'URG', code: 'URG'},
      {name: 'MNT', code: 'MNT'},
    ];
    this.selectedCity = this.cities[0];
  }

  ngOnInit(): void {
    this.getUserUrgBalance();
    this.bankStatement();
    this.event.walletMenuChange(2);
  }

  getUserUrgBalance() {
    this.endpointService.getUserUrgBalance().subscribe(res => {
      if (res.status === 0) {
        this.userBalanceInfo = res;
        if (res.balances.length > 0) {
          this.userCoins = res.balances;
          this.getUserFiat();
        } 
        this.isAddressCreated = res.address;
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }
  
  selectDefualtCoin(balances: any) {
    let coinCode = this.route.snapshot.paramMap.get('coinCode');
    if (coinCode === null || coinCode === undefined) {
      this.selectedCoin = this.userCoins != null ? this.userCoins[0] : '';
      this.selectedCoinBalance = this.userCoins != null ? this.userCoins[0].available : 0;
    } else {
      let currentCoin;
      if (coinCode === 'MNT') {
        currentCoin = this.userFiat.find((coin: any) => coin.code === coinCode);
        this.selectedCoin = currentCoin;
        this.selectedCoinBalance = currentCoin.balance;
        this.selectedCity = this.selectedCoin;
        this.isShowMNT = true;
        this.getUserFiatHistory();
      } else {
        currentCoin = balances.find((coin: any) => coin.code === coinCode);
        this.selectedCoin = currentCoin;
        this.selectedCoinBalance = currentCoin.available;
        this.selectedCity = this.selectedCoin;
        this.getInsHistory();
      }
    }
  }

  copyAlert() {
    this.messageService.add({
      severity: 'success',
      summary: 'Амжилттай',
      detail: 'Текстийг амжилттай хуулсан.'
    });
  }

  getInsHistory() {
    this.endpointService.getWithdrawHistory(this.selectedCoin.code, this.page).subscribe(res => {
      if (res.status === 0) {
        this.totalData = res.total;
        this.walletInsList = res.transfers;
        this.historySymbol = res.symbol;
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Алдаа',
          detail: res.msg
        });
      }
    });
  }

  paginate(event: any) {
    this.page = {
      'type': 'in',
      'page': event.page,
      'size': 10
    };

    this.getInsHistory();
  }

  historyPage(event: any) {
    this.page = {
      'type': 'in',
      'page': event.page,
      'size': 10
    };

    this.getUserFiatHistory();
  }
  
  selectCoin(coin: any) {
    this.selectedCoin = coin;
    this.selectedCoinBalance = coin.available;
    this.isShowMNT = coin.code === 'MNT' ? true : false;
    this.getInsHistory();
    this.getUserFiatHistory();
    if (this.isShowMNT) {
      this.selectedCoinBalance = coin.balance;
    }
  }

  

  getUserFiat() {
    this.endpointService.getMNTbalance().subscribe(res => {
      if (res.status === 0) {
        this.userFiat = res.balances;
        this.userFiat.forEach((element: any) => {
          if (element !== null || element !== undefined) {
            this.userCoins.push(element); 
          }
          this.getUserFiatHistory();
          this.selectDefualtCoin(this.userCoins);
        });
      }
    });
  }

  getUserFiatHistory() {
    this.endpointService.getMNTbalanceHistory(this.page).subscribe(res => {
      if (res.status === 0) {
        this.userFiatHistory = res.balance_history;
        this.totalMNT = res.total;
      }
    });
  }

  bankStatement() {
    this.endpointService.fiatBalanceCharge().subscribe(res => {
      if (res.status === 0) {
        this.banks = res;
      }
    })
  }

  currentIcon(code: any) {
    const localPath = '../../../../assets/images/';
    if (code === 'MNT') {
      return localPath + 'mnt.svg'
    } else {
      return localPath + 'urg_coin.webp'
    }
  }
}
