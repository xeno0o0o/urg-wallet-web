import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';
import { BLOCK_DEC } from 'src/app/shared/uri.constants';
import { Paginator } from 'primeng/paginator';

interface City {
  name: string,
  code: string
}


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  @ViewChild('pp', {static: false}) pp: Paginator | undefined;

  cities: City[] | undefined;

  selectedCity: City | undefined;
  menuItems: any = [
    { label: 'Орлого', id: 1 },
    { label: 'Зарлага', id: 2 }
  ];
  activeItem: any;
  insData: any = [];
  withdrawData: any = [];
  withdrawHistory: any = [];
  userFiatHistory: any = [];
  blockDec: number = BLOCK_DEC;
  userCoins: any = [];
  userBalanceInfo: any = [];
  selectedCoin: any;
  selectedCoinBalance: number = 0;
  isAddressCreated: boolean = false;
  totalMNT = 0;
  isShowMNT: boolean = false;

  page = {
    'type': 'in',
    'page': 0,
    'size': 10
  };
  totalData = 0;

  constructor(
    private endpointService: EndpointService,
    private messageService: MessageService,
    private event: EventService
  ) { }

  ngOnInit(): void { 
    this.activeItem = this.menuItems[0];
    this.getUserUrgBalance();
    this.getUserFiatHistory();
    this.event.walletMenuChange(4);
  }
  activateMenu(item: any, $event: any) {
    this.activeItem = item;
    this.page.page = 0;
    if (this.selectedCoin.code === 'MNT') {
      this.isShowMNT === true;
      if (this.pp) {
        this.pp.changePageToFirst($event);
      }
      this.historyPage(this.page);
    } else {
      this.isShowMNT === false;
      if (this.activeItem.id === 1) {
        this.page.type = 'in';
      } else {
        this.page.type = 'out';
      }
      if (this.pp) {
        this.pp.changePageToFirst($event);
      }
     
      this.paginate(this.page);
    }
    
  }

  getUserUrgBalance() {
    this.endpointService.getUserUrgBalance().subscribe(res => {
      if (res.status === 0) {
        this.userBalanceInfo = res;
        if (res.balances.length > 0) {
          this.userCoins = res.balances;
        } else {
          this.addURG();
        }
        this.addMNT();
        this.selectedCoin = this.userCoins != null ? this.userCoins[0] : '';
        this.selectedCoinBalance = this.userCoins != null ? this.userCoins[0].available : 0;
        this.isAddressCreated = res.address;
        this.getWithdrawHistory();
        this.getInsData();
      }
    });
  }

  getUserFiatHistory() {
    this.endpointService.getMNTbalanceHistory(this.page).subscribe(res => {
      if (res.status === 0) {
        this.userFiatHistory = res.balance_history;
        this.totalMNT = res.total;
      }
    });
  }

  addMNT() {
    const data = {
      "code": "MNT",
      "name": "Монгол Төгрөг",
      "symbol": "₮",
      "balance": 0
    };
    this.userCoins.push(data); 
  }
  addURG() {
    const data = {
      "code": "URGX",
      "name": "URG",
      "symbol": "URGX",
      "available": 0
    };
    this.userCoins.push(data);
  }

  getInsData() {
    this.page.type = 'in';
    this.endpointService.getWithdrawHistory(this.selectedCoin.code, this.page).subscribe(res => {
      if (res.status === 0) {
        this.insData = res.transfers;
        this.totalData = res.total;
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Алдаа',
          detail: res.msg
        });
      }
    });
  }

  getWithdrawHistory() {
    this.page.type = 'out';
    this.endpointService.getWithdrawHistory(this.selectedCoin.code, this.page).subscribe(res => {
      if (res.status === 0) {
        this.withdrawHistory = res.transfers;
        this.totalData = res.total;
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Алдаа',
          detail: res.msg
        });
      }
    });
  }

  paginate(event: any) {
    this.page.page = event.page;
    if (this.page.type === 'in') {
      this.getInsData();
    } else {
      this.getWithdrawHistory();
    }
  }

  historyPage(event: any) {
    
    this.page = {
      'type': this.activeItem.id === 1 ? 'in' : 'out',
      'page': event.page,
      'size': 10
    };

    this.getUserFiatHistory();
  }

  selectCoin(coin: any) {
    this.selectedCoin = coin;
    this.selectedCoinBalance = coin.available;
    this.isShowMNT = coin.code === 'MNT' ? true : false;
    if (this.isShowMNT === true) {
      this.selectedCoinBalance = coin.balance;
      this.getUserFiatHistory();
    } else {
      this.getWithdrawHistory();
      this.getInsData();
    }
  }

  currentIcon(code: any) {
    const localPath = '../../../../assets/images/';
    if (code === 'MNT') {
      return localPath + 'mnt.svg'
    } else {
      return localPath + 'urg_coin.webp'
    }
  }
}
