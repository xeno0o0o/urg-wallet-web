import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { EndpointService } from 'src/app/services/endpoint.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  forgotForm: FormGroup = new FormGroup({
    phone: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(8),
      Validators.pattern('(?=.*[0-9]).{8,}')
    ])
  });

  constructor(
    private messageService: MessageService,
    private endpointService: EndpointService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  forgot(): void {
    if (this.forgotForm.controls['phone'].invalid) {
      this.messageService.add({
        severity: 'error',
        summary: 'Амжилтгүй',
        detail: 'Та утасны дугаараа шалгана уу.'
      });
    } else {
      let data = {
        "phoneNo": this.forgotForm.controls['phone'].value
      }
      this.authService.getToken().subscribe(sul => {
        this.endpointService.forgetPassword(data).subscribe(res => {
          this.messageService.add({
            severity: res.status === 0 ? 'success' : 'info',
            summary: 'Мэдэгдэл',
            detail: res.msg
          });
          localStorage.clear();
          setTimeout(() => {
            this.router.navigate(['/login'])
          }, 1000);
        }, error => {
          this.messageService.add({
            severity: 'error',
            summary: 'Амжилтгүй',
            detail: 'Service дуудах үед алдаа гарлаа. Та түр хүлээнэ үү'
          });
        });
      });
    }
  }

}
