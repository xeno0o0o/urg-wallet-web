import { Component, OnInit, ViewChild, ElementRef, HostListener, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EndpointService } from 'src/app/services/endpoint.service';
import { IMG_PATH } from 'src/app/shared/uri.constants';

@Component({
  selector: 'app-creator',
  templateUrl: './creator.component.html',
  styleUrls: ['./creator.component.scss']
})
export class CreatorComponent implements OnInit {
  @ViewChild('imageWrap', { static: false }) currentWrap: ElementRef | undefined;

  nfts: any = [];
  publicNft: any = [];
  collections: any = [];
  subCollections: any = [];
  IMG_PATH = IMG_PATH;

  markStyle: any;
  creatorNickname: string = '';
  nicknameCode = '';
  nicknameName = '';
  creatorInfo: any = [];
  coverBk = '';

  selectedTab = 1;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.resizeSlide()
  }

  constructor(
    private cdref: ChangeDetectorRef,
    private activeRoute: ActivatedRoute,
    private endpointService: EndpointService
  ) { 
    this.activeRoute.params.subscribe(param => {
      this.creatorNickname = param.nickname;
    });
  }

  ngOnInit(): void {
    this.resizeSlide();
    this.getCreatorInfo();
  }

  ngAfterViewInit() {
    this.resizeSlide();
  }

  resizeSlide() {
    let resize = ((this.currentWrap?.nativeElement.offsetWidth * 2.78) / 10);
    this.markStyle = resize;
    this.cdref.detectChanges();
  }

  getCreatorNfts() {
    const data = {
      nickname: this.creatorNickname
    }
    this.endpointService.getCreatorNfts(data).subscribe(res => {
      if (res.status === 0) {
        this.nfts = res.nfts;
      }
    })
  }

  getPublicNfts() {
    const data = {
      nickname: this.creatorNickname,
      page: 0,
      size: 10
    }
    this.endpointService.getPublicNft(data).subscribe(res => {
      // console.log('creator nft', res)
      if (res.status === 0) {
        this.publicNft = res.mine;
      }
    })
  }

  getCreatorInfo() {
    this.endpointService.getCreatorAbout(this.creatorNickname).subscribe(res => {
      if (res.status === 0) {
        this.creatorInfo = res;
        this.getPublicNfts();
        this.getCreatorNfts();
        this.nicknameCode = this.creatorInfo.nickname.substr(this.creatorInfo.nickname.lastIndexOf('@'));
        this.nicknameName = this.creatorInfo.nickname.substr(0, this.creatorInfo.nickname.lastIndexOf('@'));
      }
    });
  }

  tabItem(tabIndex: any) {
    this.selectedTab = tabIndex;
  }

  calculateDate(startDate: string, endDate: string) {
    let isNotYet = this.getTimeDif(startDate) > 0 ? true : false;
    let isNftExpire = this.getTimeDif(endDate) > 0 ? false : true;

    
    if (isNotYet === false && isNftExpire === false) {
      return 'active'
    }  
    if (isNotYet === true && isNftExpire === false) {
      return 'soon'
    }
    if (isNotYet === false && isNftExpire === true) {
      return 'closed'
    }
    return 'soon';
  }

  getTimeDif(endDate: string) {
    let date = new Date(endDate.replace(/-/g, "/"));
    let diff = date.getTime() - Date.now();
    return Math.floor(diff / 1000);
  }

  goToSocial(url: string){
    window.open(url, '_blank');
  }
}
