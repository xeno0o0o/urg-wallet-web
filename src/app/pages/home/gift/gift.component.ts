import { Component, OnInit } from '@angular/core';
import { CountdownConfig } from 'ngx-countdown';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';
import { BLOCK_DEC, IMG_PATH } from 'src/app/shared/uri.constants';

@Component({
  selector: 'app-gift',
  templateUrl: './gift.component.html',
  styleUrls: ['./gift.component.scss']
})
export class GiftComponent implements OnInit {
  isGiftExpire: boolean = false;
  blockDec: number = BLOCK_DEC;
  IMG_PATH: string = IMG_PATH;

  giftsObj: any = [];
  gifts: any = [];
  totalData = 0;

  isShowNextPage: boolean = true;

  page = {
    'page': 0,
    'size': 8
  }


  constructor(
    private endpointService: EndpointService,
    private eventService: EventService
  ) { }

  ngOnInit(): void {
    this.giftsObj = this.eventService.getGiftList();
    if (this.giftsObj === null || this.giftsObj === undefined) {
      this.getGifts();
    } else {
      this.totalData = this.giftsObj.total;
      this.gifts = this.giftsObj.giftCards;
    }
    this.eventService.headerMenuChange(2);
  }

  getGifts() {
    this.endpointService.getGift(this.page).subscribe(res => {
      if (res.status === 0) {
        this.gifts = res.giftCards;
        this.totalData = res.total;
      }
    });
  }

  getDateConfig(card: any) {
    let conf: CountdownConfig = {
      leftTime: this.getTimeDif(card.endDate),
      format: this.getTimeDif(card.endDate) > 86400 ? 'd:HH:mm:ss' : 'HH:mm:ss',
      prettyText: (text) => {
        return text
          .split(':')
          .map((v, index) =>
            this.getTimeDif(card.endDate) > 86400 ?
              `<span class="__time_item">${v}<div class="_t_label">` +
              (index === 0 ? `Өдөр` : index === 1 ? `Цаг` : index === 2 ? `Мин` : `Сек`)
              + `</div></span>` :
              `<span class="__time_item">${v}<div class="_t_label">` +
              (index === 0 ? `Цаг` : index === 1 ? `Мин` : index === 2 ? `Сек` : `Сек`)
              + `</div></span>`
          )
          .join('');
      },
    }
    return conf;
  }

  getTimeDif(endDate: string) {
    let date = new Date(endDate.replace(/-/g, "/"));
    let diff = date.getTime() - Date.now();
    return Math.floor(diff / 1000);
  }

  currentPrice(item: any) {
    if (item.prices.length > 0) {
      return item.prices[0].price
    } else {
      return 0
    }
  }

  currentSymbol(item: any) {
    if (item.prices.length > 0) {
      return item.prices[0].symbol
    } else {
      return 0
    }
  }

  paginate(event: any) {
    this.page = {
      'page': event.page,
      'size': 8
    };

    this.getGifts();
  }
}
