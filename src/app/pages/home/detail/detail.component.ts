import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { CountdownConfig } from 'ngx-countdown';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';

import { EndpointService } from 'src/app/services/endpoint.service';
import { UserService } from 'src/app/services/user.service';
import { UuidService } from 'src/app/services/uuid.service';
import { BLOCK_DEC, bsNetworkId, IMG_PATH } from 'src/app/shared/uri.constants';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  cardId: any;
  card: any = {};
  cardCount: any = 1;

  detail: any = [];
  giftsObj: any = [];
  gifts: any = [];

  isBuyTime: boolean = false;

  subscriber: any;
  userSub: any;
  user: any;
  userUrgBalance: number = 0;

  blockDec: number = BLOCK_DEC;
  IMG_PATH: string = IMG_PATH;

  isAuth: boolean = false;
  isDialogShow: boolean = false;
  isGiftExpire: boolean = false;
  isNotYet: boolean = false;

  selectedCurrency: any;

  urgDaxVal: any = null;
  diffTimeEv: any;
  config: CountdownConfig = {};
  

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
    password: new FormControl('')
  });

  constructor(
    private endpointService: EndpointService,
    private uuidService: UuidService,
    private messageService: MessageService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    this.getURGbyDax();
  }

  ngOnInit(): void {
    this.hasBuyInq();
    this.cardId = this.route.snapshot.paramMap.get('id');
    this.getGiftsDetail(this.cardId);
    this.subscriber = this.userService.getAuthenticationState().subscribe(result => {
      this.isAuth = result;
      if (this.isAuth === true) {
        this.getUserUrgBalance();
      }
    });
    this.userSub = this.userService.userInfo.subscribe((usr: any) => { this.user = usr; });
  }

  getGiftsDetail(giftId: any) {
    const data = { 'id': giftId }
    this.endpointService.getGiftDetail(data).subscribe(res => {
      if (res.status === 0) {
        this.card = res.giftCard;
        this.selectedCurrency = res.giftCard.prices[0];
        this.getCardData();
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'info',
          detail: res.msg
        });
        setTimeout(() => {
          this.router.navigate(['/nft/list']);
        }, 1000);
      }
    });
  }

  getCardData() {
    this.isStarted(this.card.startDate);
    this.isGiftExpire = this.getTimeDif(this.card.endDate) > 0 ? false : true;
    this.config = {
      leftTime: this.getTimeDif(this.card.endDate),
      format: this.getTimeDif(this.card.endDate) > 86400 ? 'd:HH:mm:ss' : 'HH:mm:ss',
      prettyText: (text) => {
        return text
          .split(':')
          .map((v, index) =>
            this.getTimeDif(this.card.endDate) > 86400 ?
              `<span class="__time_item">${v}<div class="_t_label">` +
              (index === 0 ? `Өдөр` : index === 1 ? `Цаг` : index === 2 ? `Мин` : `Сек`)
              + `</div></span>` :
              `<span class="__time_item">${v}<div class="_t_label">` +
              (index === 0 ? `Цаг` : index === 1 ? `Мин` : index === 2 ? `Сек` : `Сек`)
              + `</div></span>`
          )
          .join('');
      },
    }
    this.diffTimeEv = this.card.startDate;
  }

  isStarted(startdate: any) {
    this.isNotYet = this.getTimeDif(startdate) > 0 ? true : false;
  }

  getTimeDif(endDate: string) {
    let date = new Date(endDate.replace(/-/g, "/"));
    let diff = date.getTime() - Date.now();
    return Math.floor(diff / 1000);
  }

  showDialog() {
    if (this.isGiftExpire) {
      this.messageService.add({
        severity: 'info',
        summary: 'Амжилтгүй',
        detail: 'Уучлаарай тус бэлгийн хугацаа дууссан байна.'
      });
    } else {
      this.isDialogShow = true;
    }
  }

  plusUrg() {
    this.cardCount++;
  }

  minusUrg() {
    if (this.cardCount > 1) {
      this.cardCount--;
    }
  }

  checkCardCount() {
    if (this.cardCount < 1 || this.cardCount === undefined || this.cardCount === null || this.cardCount === '') {
      this.cardCount = 1;
    }
  }

  hideDialog() {
    this.isDialogShow = false;
    document.body.classList.remove('p-overflow-hidden');
  }

  login() {
    if (this.loginForm) {
      localStorage.clear();
      this.authService.login(this.loginForm.getRawValue()).subscribe(response => {
        this.router.navigate(['/home']).then();
      }, err => {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'Утас болон нууц үгээ шалгана уу.'
        });
      });
    }
  }

  buyGift() {
    if (this.user) {
      if (this.user.statusCode !== 2) {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: 'Таны хэрэглэгчийн мэдээлэл баталгаажаагүй байна'
        });
        return;
      }
    } else {
      this.userService.setUserInfo();
      this.userService.userInfo.subscribe((usr: any) => { this.user = usr; });
    }
    let currentUUID;
    if (localStorage.getItem('inqI') === null || localStorage.getItem('inqI') === undefined) {
      currentUUID = this.uuidService.getUUID();
      localStorage.setItem('inqI', currentUUID);
      localStorage.setItem('cardCount', this.cardCount);
      let data = {
        "inquireId": currentUUID,
        "giftCardId": this.card.id,
        "quantity": this.cardCount,
        "tokenSymbol": this.card.prices[0].symbol,
        "bsNetworkId": bsNetworkId,
      }
      
      this.isBuyTime = true;
      this.endpointService.buyGiftCardV2(data).subscribe(res => {
        if (res.status === 0) {
          // this.checkBuyEvent(res.inquireId);
          this.messageService.add({
            severity: 'info',
            summary: 'Мэдэгдэл',
            detail: res.msg
          });
          this.isBuyTime = false;
          this.isDialogShow = false;
          localStorage.removeItem('inqI');
          localStorage.removeItem('cardCount');
        } else {
          this.isBuyTime = false;
          this.isDialogShow = false;
          localStorage.removeItem('inqI');
          localStorage.removeItem('cardCount');
          this.messageService.add({
            severity: 'info',
            summary: 'Мэдэгдэл',
            detail: res.msg
          });
        }
      });
    } else {
      this.isBuyTime = true;
      this.isDialogShow = true;
    }
  }

  hasBuyInq() {
    if (localStorage.getItem('inqI') === null || localStorage.getItem('inqI') === undefined) {
      //
      // Any other method
      //
    } else {
      let currentUUID = localStorage.getItem('inqI');
      let buyCount = localStorage.getItem('cardCount');
      this.isBuyTime = true;
      this.isDialogShow = true;
      this.checkBuy(currentUUID, buyCount);
    }
  }

  checkBuy(currentUUID: any, buyCount: any) {
    let data = {
      "inquireId": currentUUID,
      "giftCardId": this.card.id,
      "quantity": buyCount,
      "tokenSymbol": this.card.prices[0].symbol,
      "bsNetworkId": bsNetworkId,
    }
    
    this.endpointService.buyGiftCardV2(data).subscribe(res => {
      if (res.status === 0) {
        // this.checkBuyEvent(res.inquireId);
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
        localStorage.removeItem('inqI');
        localStorage.removeItem('cardCount');
      } else {
        this.isBuyTime = false;
        this.isDialogShow = false;
        localStorage.removeItem('inqI');
        localStorage.removeItem('cardCount');
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }

  checkBuyEvent(checkId: any) {
    const data = {
      "inquire-id": checkId
    }
    const work = setInterval(() => { 
      this.endpointService.buyGiftCheckV2(data).subscribe(res => {
        if (res.status === 0 && res.requestStatus === 0) {

        } else {
          clearInterval(work);
          this.isBuyTime = false;
          this.isDialogShow = false;
          localStorage.removeItem('inqI');
          this.getUserUrgBalance();
          this.messageService.add({
            severity: 'info',
            summary: 'Мэдэгдэл',
            detail: res.resultText
          });
        }
      });
    }, 10000);
  }

  getUserUrgBalance() {
    this.endpointService.getUserUrgBalance().subscribe(res => {
      if (res.status === 0) {
        let urgPrice: any = {};
        urgPrice = res.balances.find((e: any) => e.code.includes('URG'));
        console.log(urgPrice)
        this.userUrgBalance = res.address ? urgPrice.available : 0;
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }

  getURGbyDax() {
    const data = {
      "query": "query ActivePairs {\n  sys_pair(where: {is_active: {_eq: true}}, order_by: {id: asc}) {\n    id\n    symbol\n    base_max_size\n    base_min_size\n    base_tick_size\n    quote_max_size\n    quote_min_size\n    quote_tick_size\n    baseAsset {\n      id\n      code\n      name\n      scale\n      is_crypto\n      __typename\n    }\n    quoteAsset {\n      id\n      code\n      name\n      scale\n      __typename\n    }\n    price {\n      last_price\n      last_price_dt\n      __typename\n    }\n    stats24 {\n      high\n      low\n      change24h\n      vol\n      __typename\n    }\n    __typename\n  }\n}\n"
    }
    this.endpointService.getURGbyDax(data).subscribe((res: any) => {
      // .log(res.data.sys_pair)
      if (res.data) {
        const urgData = res.data.sys_pair.find((el: any) => el.symbol === 'URGMNT');
        this.urgDaxVal = urgData.price.last_price;
      }
    });
    if (this.urgDaxVal === null) {
      this.urgDaxVal = 63;
    }
  }

}
