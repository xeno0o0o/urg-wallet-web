import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountdownConfig } from 'ngx-countdown';
import { Subscriber } from 'rxjs';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';
import { BLOCK_DEC, IMG_PATH } from 'src/app/shared/uri.constants';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  giftsObj: any = [];
  gifts: any = [];
  currentDate: any;
  blockDec: number = BLOCK_DEC;
  IMG_PATH: string = IMG_PATH;

  constructor(
    private endpointService: EndpointService,
    private eventService: EventService
  ) {
  }

  ngOnInit(): void {
    this.giftsObj = this.eventService.getGiftList();
    if (this.giftsObj === null || this.giftsObj === undefined) {
      this.getGifts();
    } else {
      this.gifts = this.giftsObj.giftCards;
    }
  }

  getGifts() {
    const data = {
      'page': 0,
      'size': 10
    }
    this.endpointService.getGift(data).subscribe(res => {
      this.gifts = res.giftCards;
    });
  }

  getImage(fileName: string) {
    return this.endpointService.fileDownload(fileName);
  }

  getDateConfig(card: any) {
    let conf: CountdownConfig = {
      leftTime: this.getTimeDif(card.endDate),
      format: this.getTimeDif(card.endDate) > 86400 ? 'd:HH:mm:ss' : 'HH:mm:ss',
      prettyText: (text) => {
        return text
          .split(':')
          .map((v, index) =>
            this.getTimeDif(card.endDate) > 86400 ?
              `<span class="__time_item">${v}<div class="_t_label">` +
              (index === 0 ? `Өдөр` : index === 1 ? `Цаг` : index === 2 ? `Мин` : `Сек`)
              + `</div></span>` :
              `<span class="__time_item">${v}<div class="_t_label">` +
              (index === 0 ? `Цаг` : index === 1 ? `Мин` : index === 2 ? `Сек` : `Сек`)
              + `</div></span>`
          )
          .join('');
      },
    }
    return conf;
  }

  getTimeDif(endDate: string) {
    let date = new Date(endDate.replace(/-/g, "/"));
    let diff = date.getTime() - Date.now();
    return Math.floor(diff / 1000);
  }

  toDataURL(url: any) {
    debugger
    var reader = new FileReader();
    return reader.readAsDataURL(url);
  }
}
