import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CountdownModule } from 'ngx-countdown';

import { HomeComponent } from './home.component';
import { MainComponent } from './main/main.component';
import { GiftComponent } from './gift/gift.component';
import { DetailComponent } from './detail/detail.component';

import { HomeResolver } from 'src/app/services/Resolver/home.resolver';

import { MessageService } from 'primeng/api';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { SharedModule } from 'src/app/shared/shared.module';

import { PaginatorModule } from 'primeng/paginator';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: '',
    component: HomeComponent,
    resolve: { resolved: HomeResolver },
    children: [
      { path: 'main', component: MainComponent },
      { path: 'list', component: GiftComponent },
      { path: 'detail/:id', component: DetailComponent }
    ]
  }
];

@NgModule({
  declarations: [
    HomeComponent,
    MainComponent,
    GiftComponent,
    DetailComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    CountdownModule,
    DialogModule,
    FormsModule,
    ReactiveFormsModule,
    ToastModule,
    PaginatorModule,
    ProgressSpinnerModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  providers: [MessageService]
})
export class HomeModule { }
