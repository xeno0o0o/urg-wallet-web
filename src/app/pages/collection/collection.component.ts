import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';
import { IMG_LCL_PATH, IMG_PATH } from 'src/app/shared/uri.constants';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {
  @ViewChild('imageWrap', { static: false }) currentWrap: ElementRef | undefined;

  nfts: any;
  nftList: any = [];
  creatorNFT: any = [];
  markStyle: any;
  IMG_PATH = IMG_PATH;
  IMG_LCL_PATH = IMG_LCL_PATH;

  creatorId: any;
  subColId: any;
  collection: any;
  selectedBk: any;
  data: any = [];
  collectionId: any;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.resizeSlide()
  }

  constructor(
    private route: ActivatedRoute,
    private eventService: EventService,
    private endpointService: EndpointService,
    private activeRoute: ActivatedRoute
  ) {
    this.subColId = this.route.snapshot.paramMap.get('subId');
    this.selectedBk = 'bn2.png';
  }

  ngOnInit(): void {
    this.resizeSlide();
    this.getUrlParam();
  }

  ngAfterViewInit() {
    this.resizeSlide();
  }

  resizeSlide() {
    let resize = ((this.currentWrap?.nativeElement.offsetWidth * 2.78) / 10);
    this.markStyle = resize;
  }

  getUrlParam() {
    this.activeRoute.params.subscribe(param => {
      if (param.id) {
        this.getCollection(param.id);
      }
    });
  }

  getCollection(collectionId: any) {
    const data = {
      'collection-id': collectionId
    }
    this.endpointService.getCollection(data).subscribe(res => {
      if (res.status === 0) {
        this.nfts = res.nfts;
      }
      // console.log('res=>', res)
    });
  }

  
  calculateDate(startDate: string, endDate: string) {
    let isNotYet = this.getTimeDif(startDate) > 0 ? true : false;
    let isNftExpire = this.getTimeDif(endDate) > 0 ? false : true;

    
    if (isNotYet === false && isNftExpire === false) {
      return 'active'
    }  
    if (isNotYet === true && isNftExpire === false) {
      return 'soon'
    }
    if (isNotYet === false && isNftExpire === true) {
      return 'closed'
    }
    return 'soon';
  }

  getTimeDif(endDate: string) {
    let date = new Date(endDate.replace(/-/g, "/"));
    let diff = date.getTime() - Date.now();
    return Math.floor(diff / 1000);
  }
}
