import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { IMG_PATH } from 'src/app/shared/uri.constants';

@Component({
    selector: 'app-collectibles',
    templateUrl: './collectibles.component.html'
  })
  export class CollectiblesComponent implements OnInit {

    IMG_PATH = IMG_PATH;
    mineNfts: any = [];
    totalData: any;
    page = {
      page: 0,
      size: 4
    }

    constructor(
      private messageService: MessageService,
      private endpointService: EndpointService,
      private route: Router
    ) { }
    ngOnInit(): void {
      this.getMineNfts();
    }

    getMineNfts() {
      this.endpointService.getNftMine(this.page).subscribe(res => {
        if (res.status === 0) {
          this.mineNfts = res.mine;
          this.totalData = res.total;
        }
      })
    }

    calculateDate(startDate: string, endDate: string) {
      let isNotYet = this.getTimeDif(startDate) > 0 ? true : false;
      let isNftExpire = this.getTimeDif(endDate) > 0 ? false : true;
  
      
      if (isNotYet === false && isNftExpire === false) {
        return 'active'
      }  
      if (isNotYet === true && isNftExpire === false) {
        return 'soon'
      }
      if (isNotYet === false && isNftExpire === true) {
        return 'closed'
      }
      return 'soon';
    }
  
    getTimeDif(endDate: string) {
      let date = new Date(endDate.replace(/-/g, "/"));
      let diff = date.getTime() - Date.now();
      return Math.floor(diff / 1000);
    }

    paginate(event: any) {
      this.page.page = event.page;
      this.getMineNfts();
    }
  
}
