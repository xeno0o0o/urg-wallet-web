import { AfterViewInit, ChangeDetectorRef, ViewChild, ElementRef, HostListener, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { EventService } from 'src/app/services/event.service';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { IMG_PATH } from 'src/app/shared/uri.constants';

@Component({
  selector: 'app-mynft',
  templateUrl: './mynft.component.html'
})
export class MyNFTComponent implements OnInit, OnDestroy {
  IMG_PATH = IMG_PATH;

  active: any;
  subscribe: any;
  markStyle: any;
  isDialogShow: boolean = false;
  userBalanceInfo: any = [];
  isAddressCreated: any;
  user: any;
  menu: any = [
    'collectibles', 'created'
  ]
  @ViewChild('imageWrap', { static: false }) currentWrap: ElementRef | undefined;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.resizeSlide()
  }

  constructor(
    private eventService: EventService,
    private endpointService: EndpointService,
    private messageService: MessageService,
    private route: Router
  ) { }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
    this.getActiveTab();        
    this.getUserUrgBalance();
    this.resizeSlide();
    this.user = this.eventService.getUser();
    if (this.user.length === 0) {
      this.getUserInfo();
    }
  }

  getActiveTab() {
    if (this.route.url === '/mynft/collectibles') {
      this.active = 'collectibles';
    } else {
      this.active = 'created';
    }
  }

  getUserInfo() {
    this.endpointService.getUserInfo().subscribe((usr: any) => {
      this.user = usr;
      this.eventService.setUser(usr);
    });
  }


  activeTab(tabIndex: number) {
    this.active = tabIndex;
  }

  resizeSlide() {
    let resize = ((this.currentWrap?.nativeElement.offsetWidth * 2.78) / 10);
    this.markStyle = resize;
  }

  tabItem(url: any) {
    this.active = url;
    this.route.navigateByUrl('/mynft/'+ url)
  }

  copyAlert() {
    this.messageService.add({
      severity: 'success',
      summary: 'Амжилттай',
      detail: 'Текстийг амжилттай хуулсан.'
    });
  }

  showDialog() {
    this.isDialogShow = true;
  }
  
  hideDialog() {
    this.isDialogShow = false;
    document.body.classList.remove('p-overflow-hidden');
  }

  getUserUrgBalance() {
    this.endpointService.getUserUrgBalance().subscribe(res => {
      if (res.status === 0) {
        this.userBalanceInfo = res;
        this.isAddressCreated = res.address;
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }
}
