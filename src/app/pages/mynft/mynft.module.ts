import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MyNFTComponent} from './mynft.component'

import { QRCodeModule } from 'angularx-qrcode';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { MessageService } from 'primeng/api';
import { TabMenuModule } from 'primeng/tabmenu';
import { CollectiblesComponent } from './collectibles/collectibles.component';
import { CreatedComponent } from './created/created.component';
import { DialogModule } from 'primeng/dialog';
import { SellComponent } from './sell/sell.component';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { PaginatorModule } from 'primeng/paginator';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'collectibles',
    pathMatch: 'full',
  },
  {
    path: '',
    component: MyNFTComponent,
    children: [
      {
        path: 'collectibles',
        component: CollectiblesComponent,
      },
      {
        path: 'created',
        component: CreatedComponent,
      },
      {
        path: 'sell/:bsId/:code',
        component: SellComponent,
      },
    ]
  }
];

@NgModule({
  declarations: [
    MyNFTComponent,
    CollectiblesComponent,
    CreatedComponent,
    SellComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    QRCodeModule,
    ClipboardModule,
    TabMenuModule,
    DialogModule,
    DropdownModule,
    CalendarModule,
    PaginatorModule,
    RouterModule.forChild(routes),
  ],

  exports: [RouterModule],
  providers: [
    MessageService
  ]
})
export class MynftModule { }
