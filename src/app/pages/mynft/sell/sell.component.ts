import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EndpointService } from 'src/app/services/endpoint.service';
import { IMG_PATH } from 'src/app/shared/uri.constants';

interface City {
  name: string,
  code: string
}

@Component({
  selector: 'app-sell',
  templateUrl: './sell.component.html',
  styleUrls: ['./sell.component.scss']
})
export class SellComponent implements OnInit {
  IMG_PATH = IMG_PATH
  nftBsId: any;
  nftCode: any;

  nft:any = [];

  userCoins: any = [];
  selectedCity: City | undefined;
  sell_date: any;

  constructor(
    private activeRoute: ActivatedRoute,
    private endpointService: EndpointService
  ) { 
    this.activeRoute.params.subscribe(param => {
      this.nftBsId = param.bsId;
      this.nftCode = param.code;
    });
  }

  ngOnInit(): void {
    this.getNftDetail();
  }

  getNftDetail() {
    this.endpointService.getNftDetail(this.nftBsId, this.nftCode).subscribe(res => {
      this.nft = res;
    })
  }

}
