import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Config } from 'ng-otp-input/lib/models/config';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { EndpointService } from 'src/app/services/endpoint.service';
import { TERM } from 'src/app/shared/uri.constants';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  @ViewChild("ngOtpInput", { static: false }) ngOtpInput: any;

  config = {
    allowNumbersOnly: true,
    length: 4,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: "*",
    inputStyles: {
      width: "50px",
      height: "50px",
    },
  };

  regex1: boolean = false;
  regex2: boolean = false;
  regex3: boolean = false;
  regex4: boolean = false;
  regex5: boolean = false;

  otp: string = '';
  showOtpComponent = true;
  isChecked: boolean = false;
  secondPass: any;
  displayDialog: boolean = false;
  termDialog: boolean = false;
  time: number = 180;
  tanCode: any = null;
  interval: any;
  isSame: boolean = true;
  registForm: FormGroup = new FormGroup({
    phone: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$#^!%*&?&])[A-Za-z0-9\d$@$!#%*&?&].{8,}')]),
  });

  term: any = TERM;

  constructor(
    private authService: AuthService,
    private router: Router,
    private endpointService: EndpointService,
    private messageService: MessageService
  ) { }
  ngOnDestroy(): void {
    localStorage.clear();
  }

  ngOnInit(): void {
  }

  regist(): void {
    if (!this.isChecked) {
      this.messageService.add({
        severity: 'error',
        summary: 'Амжилтгүй',
        detail: 'Та үйлчилгээний нөхцөлтэй танилцана уу.'
      });
      return;
    }
    if (this.registForm) {
      this.authService.getToken().subscribe(res => {
        this.authService.regist(this.registForm.getRawValue()).subscribe(response => {
          if (response.status === 0) {
            this.time = 180;
            this.displayDialog = true;
            this.startTimer();
          } else if (response.status === 1) {
            this.messageService.add({
              severity: 'info',
              summary: 'Мэдэгдэл',
              detail: response.msg
            });
          }
        });
      });
    }
  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.time > 0) {
        this.time--;
      } else {
        this.displayDialog = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'Та баталгаажуулах хугацаа дууслаа.'
        });
        clearInterval(this.interval);
      }
    }, 1000)
  }

  sendTanAgain() {
    this.time = 180;
    const data = {
      'phoneNo': this.registForm.controls['phone'].value
    }
    this.endpointService.tanAgain(data).subscribe((res: any) => {
      this.messageService.add({
        severity: 'success',
        summary: 'Амжилттай',
        detail: 'Дахин баталгаажуулах код илгээлээ.'
      });
    });
  }

  confirmRegist(tanCode: string): void {
    this.endpointService.registConfirm(this.registForm.controls['phone'].value, tanCode).subscribe((res: any) => {
      if (res.status === 0) {
        this.messageService.add({
          severity: 'success',
          summary: 'Амжилттай',
          detail: res.msg
        });
        localStorage.clear();
        setTimeout(() => {
          this.router.navigate(['login']).then();
        }, 1000);
      }
    });
  }

  onOtpChange(otp: string) {
    this.otp = otp;
    if (this.otp.length === 6) {
      this.confirmRegist(otp);
    }
  }

  checkSamePassword() {
    this.isSame = (this.secondPass === this.registForm.controls['password'].value) ? false : true;
    return this.isSame;
  }

  regexCheck() {
    let pass = this.registForm.controls['password'].value;
    const pattern1 = new RegExp('[A-Za-z\d$@$!%*#?&].{8,}'); // 8 urttai
    const pattern2 = new RegExp('(?=.*[A-Z])'); // tom useg
    const pattern3 = new RegExp('(?=.*[0-9])'); // too
    const pattern4 = new RegExp('(?=.*[$@$!%*#?&])'); // temdeg
    const pattern5 = new RegExp('(?=.*[a-z])'); // jijig useg

    this.regex1 = pattern1.test(pass);
    this.regex2 = pattern2.test(pass);
    this.regex3 = pattern3.test(pass);
    this.regex4 = pattern4.test(pass);
    this.regex5 = pattern5.test(pass);
  }
  showTerm() {
    this.termDialog = true;
  }
  hideTerm() {
    this.termDialog = false;
  }
  agreeTerm() {
    this.isChecked = true;
    this.termDialog = false;
  }
}
