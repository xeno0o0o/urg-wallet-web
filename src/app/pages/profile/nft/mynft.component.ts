import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';

@Component({
    selector: 'app-mynft',
    templateUrl: './mynft.component.html'
  })
  export class MyNFTComponent implements OnInit {

    constructor(
      private messageService: MessageService,
      private endpointService: EndpointService,
      private route: Router
    ) { }
    ngOnInit(): void {

    }
}
