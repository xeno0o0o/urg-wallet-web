import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile.component';
import { GeneralComponent } from './general/general.component';
import { InfoComponent } from './info/info.component';
import { MyNFTComponent } from './nft/mynft.component';
import { ConfigComponent } from './config/config.component';
import { AccountComponent } from './account/account.component';
import { HttpClientModule } from '@angular/common/http';
import { PaginatorModule } from 'primeng/paginator';

import { MessageService } from 'primeng/api';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { FileUploadModule } from 'primeng/fileupload';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { QRCodeModule } from 'angularx-qrcode';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'general',
    pathMatch: 'full',
  },
  {
    path: '',
    component: ProfileComponent,
    children: [
      {
        path: 'general',
        component: GeneralComponent,
      },
      {
        path: 'info',
        component: InfoComponent,
      },
      {
        path: 'account',
        component: AccountComponent,
      },
      {
        path: 'nft',
        component: MyNFTComponent,
      },
      {
        path: 'config',
        component: ConfigComponent,
      }
    ]
  }
];

@NgModule({
  declarations: [
    ProfileComponent,
    InfoComponent,
    ConfigComponent,
    AccountComponent,
    GeneralComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    ToastModule,
    FileUploadModule,
    Ng2ImgMaxModule,
    PaginatorModule,
    HttpClientModule,
    QRCodeModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  providers: [MessageService]
})
export class ProfileModule { }
