import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html'
})
export class AccountComponent implements OnInit, AfterViewInit {

  isDialogShow: boolean = false;
  myBanks: any = [];
  banks: any = [];
  fiats: any = [];

  selectedBank: any;
  selectedFiat: any;

  bankForm: FormGroup = new FormGroup({
    fiat_code: new FormControl(''),
    bank_code: new FormControl(''),
    account_no: new FormControl('', [Validators.required]),
    account_name: new FormControl('', [Validators.required])
  });

  constructor(
    private endpointService: EndpointService,
    private messageService: MessageService
    ) { }


  ngOnInit(): void {
    this.getFiats();
    this.getBanks();
    this.getMyBankAcc();
  }

  ngAfterViewInit() {
    this.bankForm.reset();
  }

  getMyBankAcc() {
    this.endpointService.getMyBankList().subscribe(res => {
      if (res.status === 0) {
        this.myBanks = res.accounts;
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'Mine Bank хүсэлтэд алдаа гарлаа.'
        });
      }
    });
  }

  getBanks() {
    this.endpointService.getBanks().subscribe(res => {
      if (res.status === 0) {
        this.banks = res.banks;
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'bank хүсэлтэд алдаа гарлаа.'
        });
      }
    });
  }

  getFiats() {
    this.endpointService.getFiats().subscribe(res => {
      if (res.status === 0) {
        this.fiats = res.fiats;
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Амжилтгүй',
          detail: 'fiat хүсэлтэд алдаа гарлаа.'
        });
      }
    });
  }

  addAccount() {
    this.isDialogShow = true
  }

  hideDialog() {
    this.endpointService.getAddBank(this.bankForm.getRawValue()).subscribe(res => {
      if (res.status === 0) {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
        this.getMyBankAcc();
        this.bankForm.reset();
        this.isDialogShow = false;
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
    document.body.classList.remove('p-overflow-hidden');
  }

  removeBanks(item: any) {
    const data = {
      'account-id': item.id
    }
    this.endpointService.deleteBank(data).subscribe(res => {
      if (res.status === 0) {
        this.getMyBankAcc();
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }
}
