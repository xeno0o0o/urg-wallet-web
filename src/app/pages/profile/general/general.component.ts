import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';
import { IMG_LCL_PATH, IMG_PATH } from 'src/app/shared/uri.constants';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html'
})
export class GeneralComponent implements OnInit {

  IMG_PATH = IMG_PATH;
  IMG_LOCAL = IMG_LCL_PATH;

  propic: File | null = null;

  user: any = [];
  isHasData: boolean = false;
  nftProfileForm: FormGroup = new FormGroup({
    nickname: new FormControl('', [Validators.required]),
    bio: new FormControl('', [Validators.required]),
    fb: new FormControl('', [Validators.required]),
    twitter: new FormControl('', [Validators.required]),
    instagram: new FormControl('', [Validators.required])
  });

  hasNickName: boolean = false;
  proPicture = '';

  constructor(
    private messageService: MessageService,
    private endpointService: EndpointService,
    private eventService: EventService
  ) {}

  ngOnInit(): void {
    this.user = this.eventService.getUser();
    if (this.user.length === 0) {
      this.getUserInfo();
    } else {
      this.setValues();
    }
  }

  getUserInfo() {
    this.endpointService.getUserInfo().subscribe((usr: any) => {
      this.user = usr;
      this.eventService.setUser(this.user);
      this.setValues();
    });
  }

  setValues() {
    this.nftProfileForm.patchValue({
      nickname: isNaN(+this.user.nickname) ? this.user.nickname : '',
      bio: this.user.bio,
      fb: this.user.fb,
      twitter: this.user.twitter,
      instagram: this.user.instagram
    });
    if (isNaN(+this.user.nickname)) {
      this.hasNickName = true;
    }
    this.proPicture = this.user.profilePictureMd === 'DEFAULT' ? this.IMG_LOCAL + 'DEFAULT' : this.IMG_PATH + this.user.profilePictureMd;
  }

  updateProfile() {
    this.endpointService.updateNFTprofile(this.nftProfileForm.getRawValue()).subscribe(res => {
      if (res.status === 0) {
        this.eventService.isSaveProfile(true);
        this.eventService.setUser(this.user);
        if (this.propic) {
          this.saveProfic();
        }
        // setTimeout(() => {
        //   this.getUserInfo();
        // }, 1000);
        this.getUserInfo();
        this.messageService.add({
          severity: 'success',
          summary: 'Амжилттай',
          detail: res.msg
        });
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }

  uploadProfile(event: any) {
    this.propic = event.target.files.item(0);
    if (this.propic) {
      this.proPicture = URL.createObjectURL(this.propic)
    }
  }

  saveProfic() {
    if (this.propic)  {
      var fd = new FormData();
      fd.append('data', this.propic);
      this.endpointService.uploadNFTprofile(fd).subscribe(res => {
        if (res.status === 0) {
          
          this.getUserInfo();
        }
      });
    }
  }
}
