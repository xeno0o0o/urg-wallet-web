import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { BLOCK_DEC, IMG_PATH } from 'src/app/shared/uri.constants';
import { Ng2ImgMaxService } from 'ng2-img-max';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  fileToUpload: File | null = null;
  fileToBackUpload: File | null = null;
  fileToSelfieUpload: File | null = null;

  userBalanceInfo: any = [];
  user: any;
  isAddressCreated: boolean = false;
  isKYC1: boolean = false;
  isKYC2: boolean = false;
  isKYC3: boolean = false;
  blockDec: number = BLOCK_DEC;
  frontIMG: string = '../../assets/images/id_front_icon.svg';
  backIMG: string = '../../assets/images/id_back_icon.svg';
  selfieIMG: string = '../../assets/images/selfie_icon.svg';
  IMG_PATH: string = IMG_PATH;
  myfile: any[] = [];

  selectedCoin: any;
  selectedCoinBalance: number = 0;

  infoForm: FormGroup = new FormGroup({
    regNo: new FormControl('', [Validators.required, this.noWhitespaceValidator]),
    lastName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z\u0400-\u04FF\d-]+'), this.noWhitespaceValidator]),
    firstName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z\u0400-\u04FF\d-]+'), this.noWhitespaceValidator]),
  });
  idImg: FormGroup = new FormGroup({
    fimg: new FormControl(),
  });

  constructor(
    private endpointService: EndpointService,
    private messageService: MessageService,
    private ng2ImgMaxService: Ng2ImgMaxService
  ) { }

  ngOnInit(): void {
    this.getUserUrgBalance();
    this.getUserInfo();
  }

  noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  getUserInfo() {
    this.endpointService.getUserInfo().subscribe((usr: any) => {
      this.user = usr;
      this.checkUserInfo(usr);
    });
  }

  getUserUrgBalance() {
    this.endpointService.getUserUrgBalance().subscribe(res => {
      if (res.status === 0) {
        this.userBalanceInfo = res.balances;
        this.selectedCoin = this.userBalanceInfo[0];
        this.isAddressCreated = res.address;
      } else {
        this.messageService.add({
          severity: 'info',
          summary: 'Мэдэгдэл',
          detail: res.msg
        });
      }
    });
  }

  checkUserInfo(user: any) {
    if ('firstName' in user) {
      this.infoForm.controls['firstName'].patchValue(this.user.firstName);
    }
    if ('lastName' in user) {
      this.infoForm.controls['lastName'].patchValue(this.user.lastName);
    }
    if ('regNo' in user) {
      this.infoForm.controls['regNo'].patchValue(this.user.regNo);
    }
    if (user?.statusCode !== 0) {
      this.infoForm.disable();
    }
    if ('idBack' in user) {
      this.isKYC1 = (user && user.statusCode === 0) ? false : true;
      this.backIMG = this.isKYC1 === true ? this.IMG_PATH + user.idBack : this.backIMG;
    }
    if ('idFront' in user) {
      this.isKYC2 = (user.statusCode === 0) ? false : true;
      this.frontIMG = this.isKYC2 === true ? this.IMG_PATH + user.idFront : this.frontIMG;
    }
    if ('selfieWithIid' in user) {
      this.isKYC3 = (user.statusCode === 0) ? false : true;
      this.selfieIMG = this.isKYC3 === true ? this.IMG_PATH + user.selfieWithIid : this.selfieIMG;
    }
  }

  confirmUserInfo() {
    this.endpointService.confirmUserInfo(this.infoForm.getRawValue()).subscribe(res => {
      this.messageService.add({
        severity: res.status === 0 ? 'success' : 'info',
        summary: 'Хувийн мэдээлэл',
        detail: res.msg
      });
      setTimeout(() => {
        this.getUserInfo();
      }, 1000);
    });
  }

  uploadFiles(event: any) {
    this.fileToUpload = event.target.files.item(0);
    if (this.fileToUpload) {
      this.frontIMG = URL.createObjectURL(this.fileToUpload)
    }
  }

  uploadFilesBack(event: any) {
    this.fileToBackUpload = event.target.files.item(0);
    if (this.fileToBackUpload) {
      this.backIMG = URL.createObjectURL(this.fileToBackUpload)
    }
  }

  uploadSelfieBack(event: any) {
    this.fileToSelfieUpload = event.target.files.item(0);
    if (this.fileToSelfieUpload) {
      this.selfieIMG = URL.createObjectURL(this.fileToSelfieUpload)
    }
  }

  sendIdImg() {
    if (this.fileToUpload === null) {
      this.messageService.add({
        severity: 'info',
        summary: 'Мэдэгдэл',
        detail: 'Та иргэний үнэмлэхний урд талыг оруулна уу'
      });
      return;
    }
    if (this.fileToBackUpload === null) {
      this.messageService.add({
        severity: 'info',
        summary: 'Мэдэгдэл',
        detail: 'Та иргэний үнэмлэхний ард талыг оруулна уу'
      });
      return;
    }
    if (this.fileToSelfieUpload === null) {
      this.messageService.add({
        severity: 'info',
        summary: 'Мэдэгдэл',
        detail: 'Та иргэний үнэмлэхтэй зурагаа оруулна уу'
      });
      return;
    }
    this.ng2ImgMaxService.resize([this.fileToUpload], 2000, 1000).subscribe((result) => {
      var fd = new FormData();
      fd.append('data', result);
      if (result) {
        this.endpointService.kycFront(fd).subscribe(res => {
          if (res.body) {
            this.saveBackSideImg();
            this.messageService.add({
              severity: 'info',
              summary: 'Урд талын зураг',
              detail: res.body.msg,
            });
          }
        });
      }
    });
  }

  saveBackSideImg() {
    if (this.fileToBackUpload !== null) {
      this.ng2ImgMaxService.resize([this.fileToBackUpload], 2000, 1000).subscribe((result) => {
        var bd = new FormData();
        bd.append('data', result);
        this.endpointService.kycBack(bd).subscribe(res => {
          if (res.body) {
            this.saveSelfieSideImg();
            this.messageService.add({
              severity: 'info',
              summary: 'Ард талын зураг',
              detail: res.body.msg,
            });
          }
        });
      });
    }
  }

  saveSelfieSideImg() {
    if (this.fileToSelfieUpload !== null) {
      this.ng2ImgMaxService.resize([this.fileToSelfieUpload], 2000, 1000).subscribe((result) => {
        var sd = new FormData();
        sd.append('data', result);
        if (result) {
          this.endpointService.kycSelfie(sd).subscribe(res => {
            if (res.body) {
              this.messageService.add({
                severity: 'info',
                summary: 'Нүүр зураг',
                detail: res.body.msg,
              });
              if (res.body.status === 0) {
                this.confirmUserInfo();
              }
            }
          });
        }
      });
    }
  }

  resizeImg(img: any) {
    this.ng2ImgMaxService.resize([img], 2000, 1000).subscribe((result) => {
      return result;
    });
  }

  selectCoin(coin: any) {
    this.selectedCoin = coin;
    this.selectedCoinBalance = coin.available;
  }
}
