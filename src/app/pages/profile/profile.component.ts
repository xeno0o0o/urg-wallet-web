import { Component, OnInit, HostListener, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { EndpointService } from 'src/app/services/endpoint.service';
import { EventService } from 'src/app/services/event.service';
import { IMG_LCL_PATH, IMG_PATH } from 'src/app/shared/uri.constants';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, AfterViewInit {
  @ViewChild('imageWrap', { static: false }) currentWrap: ElementRef | undefined;

  coverpic: File | null = null;
  active: any;
  coverPath: any;
  hasCover = false;
  IMG_LOCAL = IMG_LCL_PATH;
  IMG_PATH = IMG_PATH;
  user: any = [];
  markStyle: any;

  menu: any = [
    'general', 'info', 'account', 'card', 'nft', 'config'
  ]

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.resizeSlide()
  }

  constructor(
    private router: Router,
    private eventService: EventService,
    private endpointService: EndpointService,
    private cdref: ChangeDetectorRef 
  ) {
    this.user = this.eventService.getUser();
    this.eventService.isSaveProfule.subscribe(res => {
      if (res === true) {
        this.saveProfic();
      }
    });
  }

  ngOnInit(): void {
    let urls = this.router.url;
    let currentIn = urls.split('/');
    let currentIndex = this.menu.indexOf(currentIn[currentIn.length - 1]);
    this.active = currentIndex + 1;
    if (this.user.length === 0) {
      this.getUserInfo();
    } else {
      this.checkCover();
    }
  }

  ngAfterViewInit() {
    this.resizeSlide();
  }

  activeTab(tabIndex: number) {
    this.active = tabIndex;
  }

  getUserInfo() {
    this.endpointService.getUserInfo().subscribe((usr: any) => {
      this.user = usr;
      this.eventService.setUser(usr);
      this.checkCover();
    });
  }

  resizeSlide() {
    let resize = ((this.currentWrap?.nativeElement.offsetWidth * 2.78) / 10);
    this.markStyle = (resize === undefined || resize === null) ? 400 : resize;
    this.cdref.detectChanges();
  }

  checkCover() {
    if (this.user.coverPhoto === 'DEFAULT') {
      this.coverPath = '';
      this.hasCover = false;
    } else {
      this.hasCover = true;
      this.coverPath = this.IMG_PATH + this.user.coverPhoto;
    }
  }

  saveProfic() {
    if (this.coverpic)  {
      var fd = new FormData();
      fd.append('data', this.coverpic);
      this.endpointService.uploadNFTcover(fd).subscribe(res => {
        if (res) {
          this.getUserInfo();
        }
      });
    }
  }

  uploadFiles(event: any) {
    this.coverpic = event.target.files.item(0);
    if (this.coverpic) {
      this.hasCover = true;
      this.coverPath = URL.createObjectURL(this.coverpic)
    }
  }
}
