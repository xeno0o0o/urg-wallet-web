import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { EndpointService } from 'src/app/services/endpoint.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit {

  userSub: any;
  user: any;

  changePassForm: FormGroup = new FormGroup({
    currentPwd: new FormControl('', [Validators.required]),
    newPwd: new FormControl('', [
      Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$#^!%*&?&])[A-Za-z0-9\d$@$!#%*&?&].{8,}')]),
    rePwd: new FormControl('', [Validators.required]),
  });

  isSame: boolean = false;
  isDialogShow: boolean = false;
  regex1: boolean = false;
  regex2: boolean = false;
  regex3: boolean = false;
  regex4: boolean = false;
  regex5: boolean = false;

  constructor(
    private userService: UserService,
    private endpointService: EndpointService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.getUserInfo();
  }

  getUserInfo() {
    this.endpointService.getUserInfo().subscribe((usr: any) => { this.user = usr; });
  }

  showDialog() {
    this.changePassForm.reset();
    this.isDialogShow = true;
  }

  hideDialog() {
    this.isDialogShow = false;
    document.body.classList.remove('p-overflow-hidden');
  }

  savePasswordChange() {
    this.endpointService.profileChangePass(this.changePassForm.getRawValue()).subscribe(res => {
      this.messageService.add({
        severity: res.status === 0 ? 'success' : 'info',
        summary: 'Мэдэгдэл',
        detail: res.msg
      });
      this.isDialogShow = res.status === 0 ? false : true;
    }, error => {
      this.messageService.add({
        severity: 'error',
        summary: 'Амжилтгүй',
        detail: 'Нууц үг солих үед алдаа гарлаа.'
      });
    });
  }

  checkSamePassword() {
    this.isSame = (this.changePassForm.controls['rePwd'].value === this.changePassForm.controls['newPwd'].value) ? false : true;
    return this.isSame;
  }

  regexCheck() {
    let pass = this.changePassForm.controls['newPwd'].value;
    const pattern1 = new RegExp('[A-Za-z\d$@$!%*#?&].{8,}'); // 8 urttai
    const pattern2 = new RegExp('(?=.*[A-Z])'); // tom useg
    const pattern3 = new RegExp('(?=.*[0-9])'); // too
    const pattern4 = new RegExp('(?=.*[$@$!%*#?&])'); // temdeg
    const pattern5 = new RegExp('(?=.*[a-z])'); // jijig useg

    this.regex1 = pattern1.test(pass);
    this.regex2 = pattern2.test(pass);
    this.regex3 = pattern3.test(pass);
    this.regex4 = pattern4.test(pass);
    this.regex5 = pattern5.test(pass);
  }
}
