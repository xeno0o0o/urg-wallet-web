import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError, filter, finalize, map, switchMap, take } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { AUTH_URL, WEB_ACCESS } from 'src/app/shared/uri.constants';

@Injectable({ providedIn: 'root' })
export class AuthInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(
    private router: Router,
    private userService: UserService,
    private authService: AuthService,
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const headersConfig = {
      Accept: 'application/json',
      'Access-Control-Allow-Origin': '*',
    } as any;

    if (req.headers.get('Content-Type')) {
      headersConfig['Content-Type'] = req.headers.get('Content-Type');
    }
    if (req.headers.get('responseType')) {
      headersConfig[`responseType`] = req.headers.get('responseType');
    }
    if (req.headers.get('Accept')) {
      headersConfig[`Accept`] = req.headers.get('Accept');
    }
    const token = localStorage.getItem('access_token');
    if (token) {
      headersConfig.Authorization = `Bearer ${token}`;
    }
    if (req.url.includes(AUTH_URL)) {
      headersConfig['Authorization'] = `Basic ${btoa(WEB_ACCESS)}`;
    }

    const request = req.clone({ setHeaders: headersConfig });
    return next.handle(request)
      .pipe(
        finalize(() => {
        // determine spinner hide
      }), 
        catchError((error: any) => {
          return this.handleError(error, request, next, token);
        }) as any 
      );
  }

  refreshAuth(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (this.refreshTokenInProgress) {
      return this.refreshTokenSubject
        .pipe(
          filter(result => result !== null)
          , take(1)
          , switchMap((token) => {
            if (next !== null) {
              return next.handle(this.addToken(req, token));
            } else {
              return of(true);
            }
          }),
        );
    } else {
      this.refreshTokenInProgress = true;
      this.refreshTokenSubject.next(null);
      return this.authService.refreshToken().pipe(
        switchMap((token: any) => {
          this.authService.setRefreshTokenData(token);
          this.authService.saveAccessData(token);
          this.userService.setUser(token.access_token);
          this.refreshTokenInProgress = false;
          this.refreshTokenSubject.next(token);
          if (next !== null) {
            return next.handle(this.addToken(req, token));
          } else {
            return of(true);
          }
        }),
        catchError((error: any) => {
          if (error.status === 401) {
            this.authService.logout();
          }
          return throwError(error);
        }),
      );
    }
  }

  private handleError(error: any, req: HttpRequest<any>, next: HttpHandler, token: any): Observable<any> {
    const errorObject = error;
    switch (errorObject.status) {
     case 401:
      const isLogUrl = !req.url.match(AUTH_URL);
      if (token && isLogUrl) {
        return this.refreshAuth(req, next);
      } else {
        this.authService.logout();
      }
      return throwError(error);

      default: return throwError(error);
    }
  }

  addToken(req: HttpRequest<any>, token: any): HttpRequest<any> {
    return req.clone({ setHeaders: { Authorization: 'Bearer ' + token.access_token } });
  }
}
