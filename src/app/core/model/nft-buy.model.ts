export interface NftBuyModel {
    nftCode: string,
    count: number,
    pinCode: string,
    priceId: number,
    inquireId: string
}