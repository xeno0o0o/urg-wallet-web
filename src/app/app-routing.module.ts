import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { NoAuthGuard } from './core/guards/no-auth.guard';
import { CommingComponent } from './pages/comming/comming.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'nft',
    pathMatch: 'full',
  },
  {
    path: '',
    children: [
      { path: 'gift', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) },
      { path: 'nft', loadChildren: () => import('./pages/nft/nft.module').then(m => m.NftModule) },
      { path: 'wallet', loadChildren: () => import('./pages/wallet/wallet.module').then(m => m.WalletModule), canActivate: [AuthGuard] },
      { path: 'mynft', loadChildren: () => import('./pages/mynft/mynft.module').then(m => m.MynftModule), canActivate: [AuthGuard] },
      { path: 'creator/:nickname', loadChildren: () => import('./pages/creator/creator.module').then(m => m.CreatorModule) },
      { path: 'collection/:id', loadChildren: () => import('./pages/collection/collection.module').then(m => m.CollectionModule) },
      { path: 'collection/:id/:subId', loadChildren: () => import('./pages/collection/collection.module').then(m => m.CollectionModule) },
      { path: 'profile', loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfileModule), canActivate: [AuthGuard] },
      { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) , canActivate: [NoAuthGuard]},
      { path: 'register', loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterModule), canActivate: [NoAuthGuard] },
      { path: 'forgot', loadChildren: () => import('./pages/forgot/forgot.module').then(m => m.ForgotModule) },
      { path: 'soon', component: CommingComponent },
      { path: '404', component: NotFoundComponent },
      { path: '**', redirectTo: '/nft/list' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { 
    useHash: true , 
    scrollPositionRestoration: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
